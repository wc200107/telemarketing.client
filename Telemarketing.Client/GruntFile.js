﻿module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-angular-gettext');
    grunt.loadNpmTasks('grunt-ng-constant');
    grunt.initConfig({
        nggettext_extract: {
            pot: {
                files: {
                    'Languages/template.pot': ['*.html', 'app/**/*.html', 'app/**/*.js']
                }
            },
            options: {
                extensions: {
                    htm: 'html',
                    html: 'html',
                    php: 'html',
                    phtml: 'html',
                    tml: 'html',
                    js: 'js',
                    cshtml: 'html'
                }
            }
        },
        nggettext_compile: {
            all: {
                files: {
                    'Scripts/translations.js': ['Languages/*.po']
                }
            }
        },
        ngconstant: {
            // Options for all targets
            options: {
                space: '  ',
                wrap: '(function () {\n \'use strict\';\n\n {%= __ngModule %} })();',
                name: 'telemarketingApp',
                deps: false
            },
            //donorService
            //var _BASE_API_URL = "https://svc.worldvision.ca/DonorService/api/";
            //var _BASE_API_URL = "https://stg-svc.worldvision.ca/DonorService/api/";
            // var _BASE_API_URL = "http://dev-svc.worldvision.ca/DonorGayaneService/api/";
            //var _BASE_API_URL = "http://localhost:54662/api/";

            //sponsorshipservice
            //var _BASE_API_URL = "https://svc.worldvision.ca/sponsorshipservice/api/";
            //var _BASE_API_URL = "https://stg-svc.worldvision.ca/sponsorshipservice/api/";
            //var _BASE_API_URL = "http://dev-svc.worldvision.ca/sponsorshipservicegayane/api/";
            //var _BASE_API_URL = "http://localhost:61547/api/";

            //commonservice
            //var _BASE_API_URL = "https://svc.worldvision.ca/commonservice/api/";
            //var _BASE_API_URL = "https://stg-svc.worldvision.ca/commonservice/api/";
            //var _BASE_API_URL = "http://dev-svc.worldvision.ca/commonservicegayane/api/";
            //var _BASE_API_URL = "http://localhost:59726/api/";

            // Environment targets
            local: {
                options: {
                    dest: 'App/appConstants.js'
                },
                constants: {
                    _API_URL: {
                        _BASE_DONATION_API_URL: 'https://dev-svc.worldvision.ca/DonationService/api/',
                        _BASE_DONOR_API_URL: 'https://dev-svc.worldvision.ca/DonorService/api/',
                        _BASE_SPONSORSHIP_API_URL: 'https://dev-svc.worldvision.ca/sponsorshipservice/api/',
                        _BASE_COMMON_API_URL: 'https://dev-svc.worldvision.ca/commonservice/api/',
                        _MONERIS_URL: 'https://esqa.moneris.com/HPPtoken/index.php',
                        _MONERIS_TOKEN_ID: 'htXCMFHXHGDFMAX',
                        _DONATION_TM: 'Sponsorship-TM',
                        _SPONSORSHIP_TM: 'Sponsorship-TM',
                        _MIXED_TM: 'Sponsorship-TM',
                        _SP_DESCODE: 40501,
                        _SHOW_DEDUCTION_DATE: false,
                        _SPADDON: true
                    }
                }
            },
            development: {
                options: {
                    dest: 'App/appConstants.js'
                },
                constants: {
                    _API_URL: {
                        _BASE_DONATION_API_URL: 'https://dev-svc.worldvision.ca/DonationService/api/',
                        _BASE_DONOR_API_URL: 'https://dev-svc.worldvision.ca/DonorService/api/',
                        _BASE_SPONSORSHIP_API_URL: 'https://dev-svc.worldvision.ca/sponsorshipservice/api/',
                        _BASE_COMMON_API_URL: 'https://dev-svc.worldvision.ca/commonservice/api/',
                        _MONERIS_URL: 'https://esqa.moneris.com/HPPtoken/index.php',
                        _MONERIS_TOKEN_ID: 'htXCMFHXHGDFMAX',
                        _DONATION_TM: 'Sponsorship-TM',
                        _SPONSORSHIP_TM: 'Sponsorship-TM',
                        _MIXED_TM: 'Sponsorship-TM',
                        _SP_DESCODE: 40501,
                        _SHOW_DEDUCTION_DATE: false,
                        _SPADDON: false
                    }
                }
            },
            uat: {
                options: {
                    dest: 'App/appConstants.js'
                },
                constants: {
                    _API_URL: {
                        _BASE_DONATION_API_URL: 'https://uat-svc.worldvision.ca/DonationService/api/',
                        _BASE_DONOR_API_URL: 'https://uat-svc.worldvision.ca/DonorService/api/',
                        _BASE_SPONSORSHIP_API_URL: 'https://uat-svc.worldvision.ca/sponsorshipservice/api/',
                        _BASE_COMMON_API_URL: 'https://uat-svc.worldvision.ca/commonservice/api/',
                        _MONERIS_URL: 'https://esqa.moneris.com/HPPtoken/index.php',
                        _MONERIS_TOKEN_ID: 'htXCMFHXHGDFMAX',
                        _DONATION_TM: 'Sponsorship-TM',
                        _SPONSORSHIP_TM: 'Sponsorship-TM',
                        _MIXED_TM: 'Sponsorship-TM',
                        _SP_DESCODE: 40501,
                        _SHOW_DEDUCTION_DATE: false,
                        _SPADDON: false
                    }
                }
            },
            staging: {
                options: {
                    dest: 'App/appConstants.js'
                },
                constants: {
                    _API_URL: {
                        _BASE_DONATION_API_URL: 'https://stg-svc.worldvision.ca/DonationService/api/',
                        _BASE_DONOR_API_URL: 'https://stg-svc.worldvision.ca/DonorService/api/',
                        _BASE_SPONSORSHIP_API_URL: 'https://stg-svc.worldvision.ca/sponsorshipservice/api/',
                        _BASE_COMMON_API_URL: 'https://stg-svc.worldvision.ca/commonservice/api/',
                        _MONERIS_URL: 'https://esqa.moneris.com/HPPtoken/index.php',
                        _MONERIS_TOKEN_ID: 'htXCMFHXHGDFMAX',
                        _DONATION_TM: 'Sponsorship-TM',
                        _SPONSORSHIP_TM: 'Sponsorship-TM',
                        _MIXED_TM: 'Sponsorship-TM',
                        _SP_DESCODE: 40501,
                        _SHOW_DEDUCTION_DATE: false,
                        _SPADDON: false
                    }
                }
            },
            production: {
                options: {
                    dest: 'App/appConstants.js'
                },
                constants: {
                    _API_URL: {
                        _BASE_DONATION_API_URL: 'https://svc.worldvision.ca/DonationService/api/',
                        _BASE_DONOR_API_URL: 'https://svc.worldvision.ca/DonorService/api/',
                        _BASE_SPONSORSHIP_API_URL: 'https://svc.worldvision.ca/sponsorshipservice/api/',
                        _BASE_COMMON_API_URL: 'https://svc.worldvision.ca/commonservice/api/',
                        _MONERIS_URL: 'https://www3.moneris.com/HPPtoken/index.php',
                        _MONERIS_TOKEN_ID: 'htCXETNGD7GNVKX',
                        _DONATION_TM: 'Sponsorship-TM',
                        _SPONSORSHIP_TM: 'Sponsorship-TM',
                        _MIXED_TM: 'Sponsorship-TM',
                        _SP_DESCODE: 40501,
                        _SHOW_DEDUCTION_DATE: false,
                        _SPADDON: false
                    }
                }
            }
        }
    });

    grunt.registerTask('dev', function (target) {
        if (target === 'dist') {
            return grunt.task.run([
              'build',
              'open',
              'connect:dist:keepalive'
            ]);
        }

        grunt.task.run([
          'ngconstant:development'
        ]);
    });

    grunt.registerTask('stg', [
      'ngconstant:staging'
    ]);

    grunt.registerTask('uat', [
      'ngconstant:uat'
    ]);

    grunt.registerTask('prd', [
      'ngconstant:production'
    ]);
    grunt.registerTask('local', [
      'ngconstant:local'
    ]);

};