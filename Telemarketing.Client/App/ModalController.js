﻿(function () {
    'use strict';

    angular
        .module('telemarketingApp')
        .controller('ModalController', ModalController);

    ModalController.$inject = ['$scope', '$uibModalInstance', 'modalContent'];
    function ModalController($scope, $uibModalInstance, modalContent) {
        $scope.modalContent = modalContent;
        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

})();