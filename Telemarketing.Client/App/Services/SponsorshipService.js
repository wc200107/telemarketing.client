﻿(function () {
    'use strict';

    angular
        .module('telemarketingApp')
        .factory('sponsorshipService', sponsorshipService);

    sponsorshipService.$inject = ['$http', '$q', '_API_URL', 'helperService'];

    function sponsorshipService($http, $q, _API_URL, helperService) {
        var _BASE_API_URL = _API_URL._BASE_SPONSORSHIP_API_URL;

        // Note: in the production environment, doing the $http.get after about 2 minutes of idel time seems to call into
        // a broken connection. as a hack, we repeat the call a 2nd time for specific errors.

        var _RSA_KEY = "<RSAKeyValue><Modulus>69QYJDQWH8KvwvXuvs7UoHBLSt/vjP/ZQQZanmn2+vIo1lH9dq86kMn6OjoJBnN7ybDr7RAI0lxrEiOnHvHjxCsSsJE99cRNBWIY2WPHrt0vBZq+K6bfUgmSsoFupuJ/G6ENLSBvXlV8Es1j538ZXYTqekQTz/SwHYPOa6ISzZ/8/tql1ycuWeuN/NTD+Nv99SvmkgTQ+La+pH93Go62dQUQLE2ohNCMJr+Jkr5JeP1F8R4ObSr3hCgurZSERD/KqNKhRSiPTB766/QnQW4DPbjDSbps2IDpVO9NqSr4lUvUmtS72WS43pFSSSsnt6ungnvHYvZE/ZML90ewj/In2w==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        var PRIVATE_KEY_TEXT = "<RSAKeyValue><Modulus>69QYJDQWH8KvwvXuvs7UoHBLSt/vjP/ZQQZanmn2+vIo1lH9dq86kMn6OjoJBnN7ybDr7RAI0lxrEiOnHvHjxCsSsJE99cRNBWIY2WPHrt0vBZq+K6bfUgmSsoFupuJ/G6ENLSBvXlV8Es1j538ZXYTqekQTz/SwHYPOa6ISzZ/8/tql1ycuWeuN/NTD+Nv99SvmkgTQ+La+pH93Go62dQUQLE2ohNCMJr+Jkr5JeP1F8R4ObSr3hCgurZSERD/KqNKhRSiPTB766/QnQW4DPbjDSbps2IDpVO9NqSr4lUvUmtS72WS43pFSSSsnt6ungnvHYvZE/ZML90ewj/In2w==</Modulus><Exponent>AQAB</Exponent><P>+wPR/tr+BfA7ITgOwQxmdaoq6f9OnT8etOFGUgpZhhdD/ZxsYdiBbeiXKenKjKw7O8Sj94JYJrjsP7LxT8rZc0M9xtVPb9n2GKWKpzq6OZ/uh39oQ/dkzK5G/bw2AX6xmwU83E0o//lV4u5jxr2AJ0ohsaR7qc2BCXOLq5PhpAE=</P><Q>8IMQo8SjibfSVNvTL31Q2+mJTAs4YocS3y50KbjPXcJrz+NI/FgFTU/McCMlWizsnEnrEMPqSbD4gyeNpoPv3947zKpVEjPf6XyU3Q33w6b7/ySNVPwpbdjgSSKtP1eBpurusUGwiIscLhFzhta1GqOv+BKfKqH5bbOhK86e29s=</Q><DP>7Yhq8tHG6lWvjrrec15JovUZ0P7xJKpzY1V+VupGVzay905L3bekmx4r1dTQAJiHIQLu6qwkwNfjcE/kPM7HQWSTFBINLtpCIIaek6tmSFuIvB4by51TfME6mqe9L2L1rK2jLxGxR7Fpzeoq7wmqOQPUqKbvAvteLVRzCjVRpAE=</DP><DQ>klfdlxh+Pb0JETNU0++XdMgsD4ZAP8tak0xLyFs4ah9zxGHStiDE0R7+ETmBb9Yn/o0HqALdmwtAm2VEzo43NeueTVFTRCkmVIbGeZ8XJAiGCuBt0slAWCN/jWC5M/KF5E/M/zQuO1cRxYI+3kvklJoG55ZFHQbDo5mKNwKZLMM=</DQ><InverseQ>msuDI/cyou0CwmcR5Q64ejD3x7c8pj6YV7LDeXe+CmCQHIQ8Lh+XPdZ4kHsyb2gT0HMWQ04ETa4P0tpDzACIDEeIA4eD7V3wTxSE8qWRFdSwiUVkU7tEVlFqxRZ23YdLufHx4jZQqFsQ+LCTr1jw/RrBIKYNfaBrJbJiRtUBwFo=</InverseQ><D>vEd5+rWHEFZcAvhcYwQ4Z9sfkajV1SmS50JOaPlK0TEy/2E4TmA6DszitUgOWpMqum1A/uR1VpWoejPywlb2tHB7HHLpF+VgZLLcCfuKugYm+39+4tT4qMOx1khV8nEUcBeVsHib0aHITC6k23by5mIu3eyqMLY3QWkxDWe5QWY4piJhVxucBn+9cSMlQS/zXGG1k4cU1RPSWjImRLGrgo6Ai9GOTlscrqDBy39p6Bf7Ag2UKM4uxv4bAMzNqLKrXWjxAMeb1yVlU6YrcxCSZmhjrSlGzxweCXbwTOETR0M0HYKyzZrwwJMBiayjlPp6wUVMKSAqbZgcf0VgLNL4AQ==</D></RSAKeyValue>";
        //  var _RSA_KEY = "<RSAKeyValue><Modulus>2DJMVMQNFbqv9qPcfymAkDLHCt5neXqF6lO7rwgAx374JEBVS9Zb8mh8EcoK666cuQha+UdyCcFXplNdyiRqJ8NuoHjBsF1itWl3eVLXYJeHL1tIwVEtdH6L8OYIfjWHNvyR4wKdQkFoavhTzabgUrfP5RHuXdT4+YWqYNEWmpRWtEfrSZrOhJmx76YyyqRgpaV+dEOo8Y/p8sOBBVje6eBUtO81LEgr8jjUXj1CvWtOoUh+TiUv3OMwq1LzDP0Xda+vVBmWfUbO/aiP+7ZXtnmV2FGPzw0DhIjgpU0b7UMh4XPckl3Lkcx2odK8jA390T1ifsUO5Sd0AVOwhmSLoQ==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        var _defaultSponsorship = {};
        var _serviceErrors = [];
        var _serviceResult = {};

        var searchSpecificUrl = function (url) {
            var deffered = $q.defer();
            console.log('url=', url);
            $http.get(url)
                .then(function (result) {
                    // success
                    deffered.resolve(result.data);
                },
                function (result) {
                    // error
                    if (!(typeof result.status === "undefined") && (result.status == 0 || result.status == 12152)) {
                        // hack: try a second time
                        $http.get(url)
                            .then(function (result) {
                                // success
                                deffered.resolve(result.data);
                            },
                            function (result) {
                                // error
                                deffered.reject();
                            });
                    }
                    else {
                        deffered.reject();
                    }
                });


            return deffered.promise;
        };

        var getChildInformation = function (childId) {
            var deffered = $q.defer();
            var config = {
                headers: {
                    'Accept': 'application/json'
                }
            };
            $http.get(_BASE_API_URL + "child/" + childId, config)
                .then(function (result) {
                    // success
                    deffered.resolve(result.data);
                },
                function (result) {
                    // error
                    if (!(typeof result.status === "undefined") && (result.status == 0 || result.status == 12152)) {
                        // hack: try a second time
                        $http.get(url)
                            .then(function (result) {
                                // success
                                deffered.resolve(result.data);
                            },
                            function (result) {
                                // error
                                deffered.reject();
                            });
                    }
                    else {
                        deffered.reject();
                    }
                });


            return deffered.promise;
        };

        var getDefaultSponsorship = function () {
            var deffered = $q.defer();

            var config = {
                headers: {
                    'Accept': 'application/json'
                }
            };
            $http.get(_BASE_API_URL + "sponsorship/default", config)
                .then(function (result) {
                    // success

                    // ************** override default values here.
                    // result.data.lang = "EN";
                    // result.data.objectType = "Sponsorship";

                    angular.copy(result.data, _defaultSponsorship);
                    deffered.resolve();
                },
                function (result) {
                    // error.
                    if (!(typeof result.status === "undefined") && (result.status == 0 || result.status == 12152)) {
                        // try a second time
                        $http.get(_BASE_API_URL + "sponsorship/default", config)
                            .then(function (result) {
                                // success
                                // ************** override default values here.
                                // result.data.lang = "EN";
                                // result.data.objectType = "Sponsorship";

                                angular.copy(result.data, _defaultSponsorship);
                                deffered.resolve(result.data);
                            },
                            function (result) {
                                // error
                                deffered.reject();
                            });
                    }
                    else {
                        deffered.reject();
                    }
                });

            return deffered.promise;

        };

        var lockChild = function (projectId, childNumber, lockedBy) {
            var deffered = $q.defer();
            var url = _BASE_API_URL + "child/lock/" + projectId + "-" + childNumber + "/" + lockedBy;
            $http.post(url)
                .then(function (result) {
                    // success
                    deffered.resolve();
                },
                function (result) {
                    // error
                    if (!(typeof result.status === "undefined") && (result.status == 0 || result.status == 12152)) {
                        // try a second time
                        $http.get(url)
                            .then(function (result) {
                                // success
                                deffered.resolve();
                            },
                            function (result) {
                                // error
                                var errors = [];
                                errors.push(result.message);
                                angular.copy(errors, _serviceErrors);

                                deffered.reject(result);
                            });
                    }
                    else {
                        if (!(typeof result.status === "undefined") && result.status == 400) {
                            var errors = [];
                            errors.push("Child already sponsored!");
                            angular.copy(errors, _serviceErrors);
                        }

                        deffered.reject(result);
                    }
                });

            return deffered.promise;

        };

        var unlockChild = function (projectId, childNumber, lockedBy) {
            var deffered = $q.defer();
            var url = _BASE_API_URL + "child/unlock/" + projectId + "-" + childNumber + "/" + lockedBy;

            $http.post(url)
                .then(function (result) {
                    // success
                    deffered.resolve();
                },
                function (result) {
                    // error
                    var errors = [];
                    errors.push(result.message);
                    angular.copy(errors, _serviceErrors);

                    deffered.reject(result);
                });

            return deffered.promise;

        };

        var insertSponsorship = function (newSponsorship, lockedBy) {

            var deffered = $q.defer();

            var url = _BASE_API_URL + "sponsor/" + lockedBy;
            $http.post(url, newSponsorship)
                .then(function (result) {
                    // success
                    _serviceErrors = [];
                    deffered.resolve(result.data);
                },
                function (result) {
                    // error
                    if (!(typeof result.status === "undefined") && (result.status == 0 || result.status == 12152)) {
                        // try a second time
                        $http.post(url, newSponsorship)
                            .then(function (result) {
                                // success
                                _serviceErrors = [];
                                deffered.resolve(result.data);
                            },
                            function (result) {
                                // error
                                angular.copy(parseErrors(result.data), _serviceErrors);
                                deffered.reject();
                            });
                    }
                    else {
                        angular.copy(parseErrors(result.data), _serviceErrors);
                        deffered.reject();
                    }
                });

            return deffered.promise;
        };

        var logActivity = function (logObject) {
            var deffered = $q.defer();

            var url = _BASE_API_URL + "log";

            $http.post(url, logObject)
                .then(function (result) {
                    // success
                    _serviceErrors = [];
                    deffered.resolve(result.data);
                },
                function (result) {
                    // error
                    if (!(typeof result.status === "undefined") && (result.status == 0 || result.status == 12152)) {
                        // try a second time
                        $http.post(url, logObject)
                            .then(function (result) {
                                // success
                                _serviceErrors = [];
                                deffered.resolve(result.data);
                            },
                            function (result) {
                                // error
                                angular.copy(parseErrors(result.data), _serviceErrors);
                                deffered.reject();
                            });
                    }
                    else {
                        angular.copy(parseErrors(result.data), _serviceErrors);
                        deffered.reject();
                    }
                });

            return deffered.promise;

        };

        var encrypt = function (plain) {
            var rsa = new System.Security.Cryptography.RSACryptoServiceProvider();
            rsa.FromXmlString(_RSA_KEY);

            var decryptedBytes = System.Text.Encoding.UTF8.GetBytes(plain);
            // Create a new instance of RSACryptoServiceProvider.

            // Encrypt byte array.
            var encryptedBytes = rsa.Encrypt(decryptedBytes, true);
            // Convert bytes to base64 string.
            var encryptedString = System.Convert.ToBase64String(encryptedBytes);
            return encryptedString;
        };

        var decrypt = function (encrypted) {

            var encryptedBytes = System.Convert.FromBase64String(encrypted);

            // Decrypt text
            var rsa = new System.Security.Cryptography.RSACryptoServiceProvider();
            rsa.FromXmlString(PRIVATE_KEY_TEXT);

            var plainBytes = rsa.Decrypt(encryptedBytes, true);

            // Write decrypted text
            return System.Text.Encoding.UTF8.GetString(plainBytes);
        };

        var indexOf = function (needle) {
            if (typeof Array.prototype.indexOf === 'function') {
                indexOf = Array.prototype.indexOf;
            } else {
                indexOf = function (needle) {
                    var i = -1, index = -1;

                    for (i = 0; i < this.length; i++) {
                        if (this[i] === needle) {
                            index = i;
                            break;
                        }
                    }

                    return index;
                };
            }

            return indexOf.call(this, needle);
        };

        var sendEmail = function (sponsorshipId) {
            var url = _BASE_API_URL + "emails/thankyou/" + sponsorshipId;
            return helperService.httpPostJson(url, null);
        };

        var getPledges = function () {
            var url = _BASE_API_URL + "donation/pledges";
            return helperService.httpGetJson(url);
        };

        function parseErrors(errorResponse) {
            var errors = [];
            if (typeof errorResponse.message !== "undefined") {
                errors.push(errorResponse.message);
            }

            for (var key in errorResponse.modelState) {
                for (var i = 0; i < errorResponse.modelState[key].length; i++) {
                    if (errors.indexOf(errorResponse.modelState[key][i]) == -1) {
                        errors.push(errorResponse.modelState[key][i]);
                    }

                }
            }
            return errors;
        }

        return {
            getDefaultSponsorship: getDefaultSponsorship,
            defaultSponsorship: _defaultSponsorship,
            searchSpecificUrl: searchSpecificUrl,
            serviceUrl: _BASE_API_URL + "children",
            lockChild: lockChild,
            unlockChild: unlockChild,
            insertSponsorship: insertSponsorship,
            serviceErrors: _serviceErrors,
            encrypt: encrypt,
            decrypt: decrypt,
            logActivity: logActivity,
            serviceResult: _serviceResult,
            getChildInformation: getChildInformation,
            sendEmail: sendEmail,
            getPledges: getPledges
        };
    }
})();