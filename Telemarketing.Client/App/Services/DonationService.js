﻿(function () {
    'use strict';

    angular
        .module('telemarketingApp')
        .factory('donationService', donationService);

    donationService.$inject = ['$http', '$q', '_API_URL', 'helperService'];

    function donationService($http, $q, _API_URL, helperService) {
        var _BASE_API_URL = _API_URL._BASE_DONATION_API_URL;

        var _serviceErrors = [];
        var _defaultDonation = {};

        ///==============================================

        var getDefaultDonation = function () {
            var deffered = $q.defer();

            var config = {
                headers: {
                    'Accept': 'application/json'
                }
            };
            $http.get(_BASE_API_URL + "donation/default", config)
                .then(function (result) {
                    // success

                    // ************** override default values here.
                    // result.data.lang = "EN";
                    // result.data.objectType = "Sponsorship";

                    angular.copy(result.data, _defaultDonation);
                    deffered.resolve();
                },
                function (result) {
                    // error.
                    if (!(typeof result.status === "undefined") && (result.status == 0 || result.status == 12152)) {
                        // try a second time
                        $http.get(_BASE_API_URL + "donation/default", config)
                            .then(function (result) {
                                // success
                                // ************** override default values here.
                                // result.data.lang = "EN";
                                // result.data.objectType = "Sponsorship";

                                angular.copy(result.data, _defaultDonation);
                                deffered.resolve(result.data);
                            },
                            function (result) {
                                // error
                                deffered.reject();
                            });
                    }
                    else {
                        deffered.reject();
                    }
                });

            return deffered.promise;
        };

        var insertDonation = function (newDonation, lockedBy) {
            console.log('newDonation', newDonation);

            var deffered = $q.defer();

            //var url = "http://localhost:65011/api/donation/lockedBy=" + lockedBy;
            var url = _BASE_API_URL + "donation/" + lockedBy;
            //alert('url=' + url);

            $http.post(url, newDonation)
                .then(function (result) {
                    // success
                    _serviceErrors = [];
                    deffered.resolve(result.data);
                },
                function (result) {
                    // error
                    if (!(typeof result.status === "undefined") && (result.status == 0 || result.status == 12152)) {
                        // try a second time
                        $http.post(url, newDonation)
                            .then(function (result) {
                                // success
                                _serviceErrors = [];
                                deffered.resolve(result.data);
                            },
                            function (result) {
                                // error
                                angular.copy(parseErrors(result.data), _serviceErrors);
                                deffered.reject(_serviceErrors);
                            });
                    }
                    else {
                        angular.copy(parseErrors(result.data), _serviceErrors);
                        deffered.reject(_serviceErrors);
                    }
                });

            return deffered.promise;
        };

        function getAllActivePledgeOffers() {
            var url = _BASE_API_URL + "allActivePledgeOffers";
            return helperService.httpGetJson(url);
        };

        var sendEmail = function (id) {
            var url = _BASE_API_URL + "emails/thankyou/" + id;
            return helperService.httpPostJson(url, null);
        };

        function parseErrors(errorResponse) {
            var errors = [];

            if (errorResponse != null) {
                if (typeof errorResponse.message !== "undefined") {
                    errors.push(errorResponse.message);
                }

                for (var key in errorResponse.modelState) {
                    for (var i = 0; i < errorResponse.modelState[key].length; i++) {
                        if (errors.indexOf(errorResponse.modelState[key][i]) == -1) {
                            errors.push(errorResponse.modelState[key][i]);
                        }

                    }
                }
            }
            return errors;
        };

        return {
            serviceErrors: _serviceErrors,
            defaultDonation: _defaultDonation,
            getAllActivePledgeOffers: getAllActivePledgeOffers,
            getDefaultDonation: getDefaultDonation,
            insertDonation: insertDonation,
            sendEmail: sendEmail
        };

    }
})();