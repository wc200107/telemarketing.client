﻿(function () {
    'use strict';

    angular
    .module('telemarketingApp')
    .factory('UserService', UserService);

    //A user service designed to interact with a RESTful web service 
    //to manage users within the system.
    UserService.$inject = ['$http', '_API_URL', 'helperService'];
    function UserService($http, _API_URL, helperService) {
        var _BASE_API_URL = _API_URL._BASE_SPONSORSHIP_API_URL;
        var service = {};

        service.GetByUsername = GetByUsername;

        return service;

        function GetByUsername(username) {
            var url = _BASE_API_URL + 'user/' + username;
            return helperService.httpGetJson(url);
        }

    }

})();