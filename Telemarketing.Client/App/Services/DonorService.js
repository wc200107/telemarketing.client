﻿(function () {
    'use strict';

    angular
        .module('telemarketingApp')
        .factory('donorService', donorService);

    donorService.$inject = ['$http', '$q', '_API_URL', 'helperService'];

    function donorService($http, $q, _API_URL, helperService) {
        var _BASE_API_URL = _API_URL._BASE_DONOR_API_URL;

        var _serviceErrors = [];

        return {
            getDonor: getDonor,
            //getCreateCreditCardUrl: getCreateCreditCardUrl,
            getDonorPledges: getDonorPledges,
            //getToken: getToken,
            postDatakeyPermanent: postDatakeyPermanent
        };

        ///==============================================

        function getDonor(donorId) {
            var url = _BASE_API_URL + "/donorinfo?donorId=" + donorId;
            return helperService.httpGetJson(url);
        }

        //function getCreateCreditCardUrl(changeId) {
        //    var url = _BASE_API_URL + "tokenex/url/osp" + changeId;
        //    return helperService.httpGetJson(url);
        //}

        function getDonorPledges(donorId, languageCode) {
            var url = _BASE_API_URL + "donor/authorizations?dcssdonorid=" + donorId + "&languageCode=" + languageCode;
            return helperService.httpGetJson(url);
        }

        //function getToken(data) {
        //    var url = _BASE_API_URL + "tokenex/bw/tokenize/" + data;
        //    return helperService.httpGetJson(url);
        //}

        function postDatakeyPermanent(datakey) {
            console.log("the datakey inside:", datakey);
            var model = {
                datakey: datakey
            };

            var deffered = $q.defer();
            var config = {
                headers: { 'Accept': 'application/json', 'Content-type': 'application/json' }
            };

            $http.post(_BASE_API_URL + "datakey/permanent", model, config)
                .success(function (data, status) {
                    angular.copy([], _serviceErrors);
                    deffered.resolve(data, status);
                })
                .error(function (data, status) {
                    angular.copy(helperService.parseErrors(data), _serviceErrors);
                    deffered.reject(data, status);
                });
            return deffered.promise;

        }
    }
})();