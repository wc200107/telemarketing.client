﻿(function () {
    'use strict';

    angular
        .module('telemarketingApp')
        .factory('commonService', commonService);

    commonService.$inject = ['$http', '$q', '_API_URL', 'helperService'];

    function commonService($http, $q, _API_URL, helperService) {
        var _BASE_API_URL = _API_URL._BASE_COMMON_API_URL;

        var _serviceErrors = [];

        return {
            getChore: getChore,
            getPlay: getPlay,
            getSubject: getSubject
        };

        ///==============================================

        function getChore(dcssCode, lang) {
            var url = _BASE_API_URL + "/childchore/" + dcssCode + "/" + lang;
            return helperService.httpGetJson(url);
        }

        function getPlay(dcssCode, lang) {
            var url = _BASE_API_URL + "/childplay/" + dcssCode + "/" + lang;
            return helperService.httpGetJson(url);
        }

        function getSubject(dcssCode, lang) {
            var url = _BASE_API_URL + "/childsubject/" + dcssCode + "/" + lang;
            return helperService.httpGetJson(url);
        }

    }
})();