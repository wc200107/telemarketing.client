﻿(function () {
    'use strict';

    angular
    .module('telemarketingApp')
    .factory('AuthenticationService', AuthenticationService);

    //The authentication service contains methods for authenticating a user, setting credentials and clearing credentials 
    //from the HTTP "Authorization" headers used by the AngularJS $http service, so effectively logging in and out.
    AuthenticationService.$inject = ['$http', '$cookieStore', '$rootScope', '$timeout', 'UserService', 'gettextCatalog'];
    function AuthenticationService($http, $cookieStore, $rootScope, $timeout, UserService, gettextCatalog) {
        var service = {};

        service.Login = Login;
        service.SetCredentials = SetCredentials;
        service.ClearCredentials = ClearCredentials;

        return service;

        function Login(username, password, callback) {
            //debugger;
            var response;
            UserService.GetByUsername(username)
            .then(function (user) {
                //debugger;
                if (user !== null && user.password === Sha1.hash(password).toUpperCase()) {
                    response = { success: true };
                } else {
                    response = { success: false, message: gettextCatalog.getString('The user name and password you have entered is not valid; please try again.') };
                }
                callback(response);
            });
        }

        function SetCredentials(username, password) {
            var authdata = Base64.encode(username + ':' + password);

            $rootScope.globals = {
                currentUser: {
                    username: username,
                    authdata: authdata
                }
            };

            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
            $cookieStore.put('globals', $rootScope.globals);
        }

        function ClearCredentials() {
            $rootScope.globals = {};
            $cookieStore.remove('globals');
            $http.defaults.headers.common.Authorization = 'Basic ';
        }
    }

})();