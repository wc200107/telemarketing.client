﻿(function () {
    'use strict';

    angular
        .module('telemarketingApp')
        .controller('aboutController', aboutController);

    aboutController.$inject = ['$scope', '$location', '$state', '$stateParams', '_API_URL'];
    function aboutController($scope, $location, $state, $stateParams, _API_URL) {
        var va = this;
        va.donationService = _API_URL._BASE_DONATION_API_URL;
        va.donorService = _API_URL._BASE_DONOR_API_URL;
        va.sponsorshipService = _API_URL._BASE_SPONSORSHIP_API_URL;
        va.commonService = _API_URL._BASE_COMMON_API_URL;
        va.moneris = _API_URL._MONERIS_URL;
        va.spAddon = _API_URL._SPADDON;
    }

})();