﻿(function () {
    'use strict';

    angular
        .module('telemarketingApp')
        .controller('sponsorshipController', sponsorship);

    sponsorship.$inject = ['$scope', '$sce', '$locale', '$state', '$stateParams', '$uibModal', '$window', '$filter',
            'sponsorshipService', 'donorService', 'donationService', 'gettextCatalog', '_API_URL'];
    function sponsorship($scope, $sce, $locale, $state, $stateParams, $uibModal, $window, $filter,
            sponsorshipService, donorService, donationService, gettextCatalog, _API_URL) {

        var vm = this;
        // calculate a session offset.
        vm.offset = Math.floor(Math.random() * 500);
        vm.showSponsorshipAddon = _API_URL._SPADDON;
        vm.spDescode = _API_URL._SP_DESCODE;
        vm.showDeductionDate = _API_URL._SHOW_DEDUCTION_DATE;
        vm.spAmount = 39;
        vm.donationAmount = 10;
        vm.donationPledgeMax = 5;
        vm.maxAmount = 9999;
        vm.lang = "en";
        vm.motcode = "";
        vm.donorid = "";
        vm.TxChildren = "";
        vm.TxPledges = "";
        vm.statuscode = "";
        vm.payment = "";
        vm.selectedPaymentMethod = {};
        vm.isInitalLoad = true;
        vm.showSummary = false;

        vm.serviceAvailable = false;
        vm.isBusy = true;
        vm.showErrors = false;
        vm.errors = [];
        vm.errorAmountMessage = '';
        vm.selectedChildren = [];

        vm.totalCount = 0;
        vm.totalPages = 0;
        vm.currentPage = 0;
        vm.children = {};
        vm.prevPageUrl = "";
        vm.nextPageUrl = "";
        vm.paymentMethods = [];
        vm.allPaymentMethods = [];
        vm.paymentMethodsWithoutBank = [];

        vm.selectedPledgeType = "";
        vm.pledgeOffers = {};
        vm.donorInfo = "";

        vm.includeFrench = true;
        vm.includeEnglish = true;
        vm.showInformationForm = false;
        vm.contentUrl = 'RemoteContent/OspContent.html';
        vm.contentUrlFooter = 'RemoteContent/OspContentFooter.html';
        vm.showMarketingInfo = false;
        vm.showDonorInformation = true;

        vm.provinces = [
            "AB", "BC", "MB", "NB", "NL", "NS", "NT", "NU", "ON", "PE", "QC", "SK", "YT"
        ];
        vm.selectPayment = selectPayment;
        vm.newBankAccount = newBankAccount;
        vm.selectPaymentMethod = selectPaymentMethod;
        vm.selectCheque = selectCheque;
        vm.isUndefinedOrNull = isUndefinedOrNull;

        vm.editableDonation = {};
        vm.editableDonationDonorType = getDonorType;
        vm.isMonerisError = false;
        vm.monerisErrorMessage = "";
        vm.savedaccount = "";

        vm.currentYear = new Date().getFullYear();
        vm.currentMonth = new Date().getMonth() + 1;
        vm.months = $locale.DATETIME_FORMATS.MONTH;
        vm.ccinfo = { type: undefined, mask: undefined, withdrawalDay: undefined, frequency: undefined };

        vm.addonDonation = false;
        vm.pledges = [];
        vm.addonCount = 0;
        vm.addonPledges = [];
        vm.allPledgesAdded = false;

        vm.selectedPledgeOffer = {};
        vm.selectedPledgeOffer.pledge_Type_Code = vm.isUndefinedOrNull($stateParams.pledgetype) ? '' : $stateParams.pledgetype.toUpperCase();
        vm.selectedPledgeOffer.designation_Code = $stateParams.descode;
        vm.selectedDollarHandle = {};
        vm.selectedSgDesignation = {};
        vm.selectedPOBusinessDescription = '';
        vm.prevPaymentFrequencyCode = '';
        vm.newPaymentMethod = false;
        vm.overallPaymentFreq='';

        vm.savedPledges = [];
        vm.pledgeOfferBusinessDescription = []; // dropdown for displaying unique pledge offer business description
        vm.sgDropDown = []; // dropdown for displaying SG designation description
        vm.isSg = false;

        var dt = new Date();
        $scope.currentYear = dt.getFullYear();
        vm.ccDeductionDate = dt;
        vm.ccWithdrawalDay = dt.getDate();

        var currentDate = new Date();
        currentDate.setHours(0);
        vm.minDeductionDate = currentDate;
        vm.maxDeductionDate = addMonths(new Date(), 2);

        vm.dayRange = [];
        for (var i = 1; i < 29; i++) {
            vm.dayRange.push(i);
        }

        $scope.$watch("vm.editableDonation.person.addressLine1", function () {
            if (vm.editableDonation != undefined && vm.editableDonation.person != undefined && vm.editableDonation.person.addressLine1 != null && vm.editableDonation.person.addressLine1 != undefined) {
                vm.editableDonation.person.addressLine1 = vm.editableDonation.person.addressLine1.replace(/<\/?[^>]+(>|$)/g, "");
            }
        });

        $scope.$watch("vm.editableDonation.person.addressLine2", function () {
            if (vm.editableDonation != undefined && vm.editableDonation.person != undefined && vm.editableDonation.person.addressLine2 != null && vm.editableDonation.person.addressLine2 != undefined) {
                vm.editableDonation.person.addressLine2 = vm.editableDonation.person.addressLine2.replace(/<\/?[^>]+(>|$)/g, "");
            }
        });

        $scope.$watch("vm.editableDonation.person.city", function () {
            if (vm.editableDonation != undefined && vm.editableDonation.person != undefined && vm.editableDonation.person.city != null && vm.editableDonation.person.city != undefined) {
                vm.editableDonation.person.city = vm.editableDonation.person.city.replace(/<\/?[^>]+(>|$)/g, "");
            }
        });

        $scope.$on("$stateChangeSuccess", function viewParams() {
            vm.lang = $stateParams.lang;
            vm.motcode = $stateParams.motcode;
            vm.donorid = $stateParams.donorid;
            vm.TxChildren = $stateParams.children;
            vm.statuscode = ($stateParams.statuscode != undefined ? $stateParams.statuscode : vm.statuscode);
            vm.payment = ($stateParams.payment != undefined ? $stateParams.payment : vm.payment);
            vm.offset = ($stateParams.offset != undefined ? $stateParams.offset : vm.offset);
            vm.donorInfo = ($stateParams.donorInfo != undefined ? $stateParams.donorInfo : vm.donorInfo);
            vm.TxPledges = ($stateParams.donationPledges != undefined ? $stateParams.donationPledges : vm.donationPledges);
            //debugger;
            vm.searchParams = {
                gender: ($stateParams.gender == "" || $stateParams.gender == null ? "-" : $stateParams.gender),
                age: ($stateParams.ageMin == "" || $stateParams.ageMin == null ? -1 : parseInt($stateParams.ageMin)),
                maxage: ($stateParams.ageMax == "" || $stateParams.ageMax == null ? -1 : parseInt($stateParams.ageMax)),
                month: ($stateParams.month == "" || $stateParams.month == null ? -1 : parseInt($stateParams.month)),
                day: ($stateParams.day == "" || $stateParams.day == null ? -1 : parseInt($stateParams.day)),
                country: ($stateParams.country == "" || $stateParams.country == null ? "-" : $stateParams.country)
            };
            vm.ignoreMotivation = ($stateParams.ignoreMotivation == "" || $stateParams.ignoreMotivation == null ? false : ($stateParams.ignoreMotivation == "False" ? false : ($stateParams.ignoreMotivation == "True" ? true : $stateParams.ignoreMotivation)));
            if (($stateParams.payment != "" && $stateParams.payment != undefined) || ($stateParams.statuscode != "" && $stateParams.statuscode != undefined)) {
                //go to the sponsorselect page
                $state.go('sponsorselect',
                    {
                        donorInfo: vm.donorInfo,
                        donorid: vm.donorid,
                        motcode: vm.motcode,
                        lang: vm.lang,
                        children: vm.TxChildren,
                        offset: vm.offset,
                        country: vm.searchParams.country,
                        gender: vm.searchParams.gender,
                        month: vm.searchParams.month,
                        day: vm.searchParams.day,
                        ageMin: vm.searchParams.age,
                        ageMax: vm.searchParams.ageMax,
                        ignoreMotivation: vm.ignoreMotivation,
                        donationPledges: vm.TxPledges,
                        payment: vm.payment,
                        pledgetype: vm.selectedPledgeOffer.pledge_Type_Code,
                        descode: vm.selectedPledgeOffer.designation_Code
                    });
            }

            gettextCatalog.setCurrentLanguage(vm.lang);
            if (vm.lang == 'fr') {
                vm.contentUrl = 'RemoteContent/OspContent_fr.html';
                vm.contentUrlFooter = 'RemoteContent/OspContentFooter_fr.html';
            }
            else if (vm.lang == 'zh') {
                vm.contentUrl = 'RemoteContent/OspContent_zh.html';
                vm.contentUrlFooter = 'RemoteContent/OspContentFooter_zh.html';
            }

            vm.titles = [
                { "value": "01", "text": gettextCatalog.getString("Mr.") },
                { "value": "02", "text": gettextCatalog.getString("Mrs.") },
                { "value": "04", "text": gettextCatalog.getString("Miss") },
                { "value": "05", "text": gettextCatalog.getString("Ms") },
            ];

            //getting pledge offers
            donationService.getAllActivePledgeOffers()
            .then(function (data) {
                var isSgAdded = false;
                var sgFirstItem = {};
                var isSgFirstItem = true;
                vm.pledgeOffers = data;
                for (var i in vm.pledgeOffers) {
                    if (!vm.isUndefinedOrNull(vm.pledgeOffers[i].pledge_Type_Code)) {
                        // set selectedPledgeOffer
                        if (vm.pledgeOffers[i].pledge_Type_Code == vm.selectedPledgeOffer.pledge_Type_Code &&
                                vm.pledgeOffers[i].designation_Code == vm.selectedPledgeOffer.designation_Code) {

                            setSelectedPledgeOfferWithSg(vm.pledgeOffers[i]);

                            // populate business description drop down if SG
                            vm.selectedPOBusinessDescription = vm.pledgeOffers[i].business_Pledge_Description;
                            if (vm.selectedPledgeOffer.pledge_Type_Code == 'SG' && !isSgAdded) {
                                vm.pledgeOfferBusinessDescription.push(vm.pledgeOffers[i].business_Pledge_Description);
                                isSgAdded = true;
                            }
                        }
                        // populate business description drop down if non SG
                        if (vm.pledgeOffers[i].pledge_Type_Code != 'SG') {
                            if (vm.pledgeOffers[i].pledge_Type_Code == 'SP') {
                                vm.spDescode = vm.pledgeOffers[i].designation_Code;
                                vm.spAmount = vm.pledgeOffers[i].pledgeOffersDollarHandles[0].dollar_Handle1;
                            }
                            vm.pledgeOfferBusinessDescription.push(vm.pledgeOffers[i].business_Pledge_Description);
                        }
                        else {
                            // pledge_Type_code = SG
                            vm.sgDropDown.push(vm.pledgeOffers[i]);
                            if (isSgFirstItem) {
                                sgFirstItem = vm.pledgeOffers[i];
                                isSgFirstItem = false;
                            }
                        }
                    }
                }
                if (!isSgAdded) {
                    vm.pledgeOfferBusinessDescription.push(sgFirstItem.business_Pledge_Description);
                }
            },
            function (error) {
            });

            //get children
            //sponsorshipService.getDefaultSponsorship()
            donationService.getDefaultDonation()
            .then(function () {
                // success
                vm.donation = donationService.defaultDonation;
                console.log('vm.donation', vm.donation);
                vm.editableDonation = angular.copy(vm.donation);
                vm.editableDonation.paymentType = undefined;
                vm.editableDonation.sponsorshipDonations = [];

                //populate donor info
                if (vm.donorid != "" && vm.donorid != undefined) {
                    vm.editableDonation.person.existingDonor = 'Y';
                    vm.editableDonation.person.donorid = vm.donorid.replace(/\D/g, '');
                }

                populateCCInfo();

                //resetSearchParameters();
                // invoke the first search, including the lookups
                searchChildren();

                //get donor information
                if (vm.donorid != "" && vm.donorid != undefined) {
                    var encryped = sponsorshipService.encrypt(vm.donorid);
                    donorService.getDonor(encryped)
                        .then(function (response) {
                            // success
                            vm.fillForm(response);

                            if (vm.donorInfo != "") {
                                //populate donor from tokenEx
                                var d = Base64.decode(vm.donorInfo);
                                //d = sponsorshipService.decrypt(d);
                                var info = d.split('||');
                                if (info.length >= 16) {
                                    vm.fillFormArray(info);
                                }
                            }

                            donorService.getDonorPledges(encryped, vm.lang)
                            .then(function (data) {
                                console.log('getDonorPledge, data=', data);
                                // success
                                vm.allPaymentMethods = data.authorizationMethods;
                                initializePaymentMethods();
                                getPaymentMethodDropdown(vm.selectedPledgeOffer.pledge_Type_Code, vm.selectedDollarHandle.payment_Frequency_Code);

                                vm.serviceAvailable = true;
                            },
                            function (response) {
                                // error
                                vm.serviceAvailable = false;
                            })
                            .then(function () {
                                vm.isBusy = false;
                                vm.isInitalLoad = false;
                            });
                        },
                        function (response) {
                            // error
                            alert('Invalid donor id. Please fix the URL and try again');
                        })
                        .then(function () {
                        });
                }
                //vm.serviceAvailable = true;
            },
            function () {
                // error
                //vm.serviceAvailable = false;
                //vm.isBusy = false;
            })
            .then(function () {

            });

            //getting pledges for addons
            sponsorshipService.getPledges()
            .then(function (data) {
                vm.pledges = $filter('filter')(data, { language: vm.lang });

                //now get already existing pledges populated
                getSelectedPledges();
            },
            function (error) {
                vm.serviceAvailable = false;
                vm.isBusy = false;
            });

        });

        // --------------------------------------------------------
        // used by Next and Prev buttons
        vm.showChild = function (url) {
            vm.errors = [];
            vm.isBusy = true;
            sponsorshipService.searchSpecificUrl(url)
                .then(function (data) {
                    // success
                    vm.currentPage = angular.copy(data.currentPage);
                    vm.children = angular.copy(data.results);
                    vm.prevPageUrl = angular.copy(data.prevPageUrl);
                    vm.nextPageUrl = angular.copy(data.nextPageUrl);

                    vm.serviceAvailable = true;
                },
                function () {
                    // error
                    console.log("childsearch error: [" + sponsorshipService.serviceResult + "]");
                    vm.serviceAvailable = false;
                })
                .then(function () {
                    vm.isBusy = false;
                });
        };

        // called when dropdowns change in value
        vm.searchChildren = function (searchParams) {
            //console.log(searchParams);
            searchChildren();
        };

        vm.resetSearch = function () {
            resetSearchParameters()
        };

        function S4() {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        }

        vm.criteriaMatch = function (array, criteria) {
            return array.filter(function (item) {
                return (item.designation === criteria);
            });
        };

        vm.dataIndex = function (guid) {
            for (var i = 0; i < vm.addonPledges.length; i++) {
                if (vm.addonPledges[i].guid === guid) {
                    return i + 1;
                    break;
                }
            }
        }

        vm.onDesChanged = function (guid, data) {
            for (var i in vm.addonPledges) {
                if (vm.addonPledges[i].guid === guid) {
                    var result = vm.criteriaMatch(vm.pledges, data);
                    vm.addonPledges[i].designation = result[0].designation;
                    vm.addonPledges[i].editdesignation = result[0].designation;
                }
            }
        }

        vm.addAddonPledge = function () {
            for (var i in vm.pledges) {
                var result = vm.criteriaMatch(vm.addonPledges, vm.pledges[i].designation);// $filter('criteriaMatch')(vm.addonPledges, vm.pledges[i].designation);
                if (result == null || result.length == 0) {
                    vm.addonPledges.push({
                        guid: (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase(),
                        designation: vm.pledges[i].designation,
                        amount: vm.donationAmount,
                        editdesignation: vm.pledges[i].designation
                    });
                    break;
                }
            }
            vm.allPledgesAdded = (vm.addonPledges.length == vm.pledges.length) || vm.donationPledgeMax == vm.addonPledges.length;
        };

        vm.addExistingPledge = function (designation, amount, editeddesignation) {
            vm.addonPledges.push({
                guid: (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase(),
                designation: designation,
                amount: amount,
                editdesignation: editeddesignation
            });

            vm.allPledgesAdded = (vm.addonPledges.length == vm.pledges.length) || vm.donationPledgeMax == vm.addonPledges.length;
        };

        vm.removePledge = function (guid) {
            for (var i = 0; i < vm.addonPledges.length; i++) {
                if (vm.addonPledges[i].guid === guid) {
                    vm.addonPledges.splice(i, 1);
                    vm.allPledgesAdded = (vm.addonPledges.length == vm.pledges.length) || vm.donationPledgeMax == vm.addonPledges.length;
                    break;
                }
            }
        }

        vm.AddFirstPledge = function (form) {
            if (vm.addonDonation) {
                if (vm.addonPledges.length == 0) {
                    vm.addAddonPledge();
                }
                form.$show();
            }
            else {
                vm.addonPledges = [];
                vm.allPledgesAdded = false;
                form.$cancel();
            }
        }

        vm.checkFrequency = function (data, index) {
            data.dollar_Handle_Custom = vm.savedPledges[index].dollarHandle.dollar_Handle_Custom;
            vm.savedPledges[index].dollarHandle = data;

            if (vm.editableDonation.paymentType == 'B' && data.payment_Frequency_Code == 'O') {
                return gettextCatalog.getString("One Time Payment cannot be done via bank.");
            }
            getPaymentMethodDropdown(vm.savedPledges[index].pledge.pledge_Type_Code, vm.savedPledges[index].dollarHandle.payment_Frequency_Code);
            vm.overallPaymentFreq = vm.savedPledges[0].dollarHandle.payment_Frequency_Description;
            vm.savedPledges[index].pledge.pledge_Type_Code = data.payment_Frequency_Code == 'O' ? 'SG'
                : getPledgeTypeCode(vm.selectedPledgeOffer.designation_Code);
        }

        vm.checkAmount = function (data, index) {
            var minAmount = vm.savedPledges[index].dollarHandle.minimum_Dollar_Amount;
            var isnum = /^\d+$/.test(data);
            if (!isnum) {
                return gettextCatalog.getString("Invalid Amount");
            }
            if (parseInt(data) < minAmount || parseInt(data) > vm.maxAmount) {
                return gettextCatalog.getString("Amount should be > " + minAmount + " and < " + vm.maxAmount);
            }
        }

        vm.checkDesignation = function (data) {
            var isnum = /^\d+$/.test(data);
            if (!isnum || data.length > 6 || parseInt(data) == 0) {
                return gettextCatalog.getString("Invalid Designation");
            }
        }

        vm.showPledge = function (data) {
            var result = vm.criteriaMatch(vm.pledges, data);// $filter('vm.criteriaMatch')(vm.pledges, data);
            if (result != null && result.length > 0) {
                return result[0].type + ' - ' + result[0].name;
            }
            return '';
        }

        vm.removeChild = function (child) {
            //remove from the list of SP children
            for (var i = 0; i < vm.selectedChildren.length; i++) {
                if (vm.selectedChildren[i].child === child) {
                    vm.selectedChildren.splice(i, 1);
                    break;
                }
            }

            vm.findAnotherChild(child.projectId, child.childNumber, vm.offset)
            vm.showSummary = vm.selectedChildren.length > 0 || vm.savedPledges.length > 0;
            vm.showInformationForm = vm.selectedChildren.length > 0 || vm.savedPledges.length > 0;
        };

        vm.readyToSponsor = function (projectId, childNumber, lockedBy) {
            // check if child can be locked properly
            vm.isBusy = true;
            sponsorshipService.lockChild(projectId, childNumber, lockedBy)
                .then(function (data) {
                    vm.selectedChildren.push({ child: vm.children[0] });
                    vm.showInformationForm = true;
                    vm.showSummary = true;
                },
                function (result) {
                    // error
                    vm.errors = sponsorshipService.serviceErrors;
                    vm.showInformationForm = false;
                    vm.showSummary = false;
                })
                .then(function () {
                    vm.isBusy = false;
                });
        };


        vm.totalSpAmounts = function () {
            return vm.selectedChildren.length * vm.spAmount;
        }

        vm.totalNonSpAmounts = function () {
            var total = 0;
            for (var i = 0; i < vm.savedPledges.length; i++) {
                total += parseInt(vm.savedPledges[i].dollarHandle.dollar_Handle_Custom);
            }

            for (var i = 0; i < vm.addonPledges.length; i++) {
                total += parseInt(vm.addonPledges[i].amount);
            }
            return total;
        }

        vm.totalAmounts = function () {
            return vm.totalSpAmounts() + vm.totalNonSpAmounts();
        }

        vm.findAnotherChild = function (projectId, childNumber, lockedBy) {
            vm.isBusy = true;
            vm.errors = [];
            if (projectId != '') {
                sponsorshipService.unlockChild(projectId, childNumber, lockedBy)
                    .then(function (data) {
                        // success
                    },
                    function (result) {
                        // error
                        vm.errors = sponsorshipService.serviceErrors;
                    })
                    .then(function () {
                        if (vm.selectedChildren.length == 0) {
                            vm.searchChildren(vm.searchParams);
                            vm.showInformationForm = false;
                        }
                        vm.isBusy = false;
                    });
            }
            else {
                vm.searchChildren(vm.searchParams);
                vm.showInformationForm = false;
                //vm.isBusy = false;
            }

            vm.TxChildren = "";
            vm.showSummary = false;
        }

        vm.openPopup = function () {
            jQuery('[data-toggle="popover"]').popover('show');
        }

        vm.validateSummaryPledges = function (isInvalid) {
            if (isInvalid) {
                return false;
            }
        }

        vm.submitForm = function (isInvalid, lockedBy, changedNotSave) {
            resetErrors();
            vm.savedaccount = "";
            if (changedNotSave) {
                vm.errors.push(gettextCatalog.getString('Please save or verify your pledge changes then click submit.'))
            }

            if (vm.selectedChildren.length == 0 && vm.savedPledges.length == 0) {
                vm.errors.push(gettextCatalog.getString('Please sponsor a child or donate a pledge'));
            }

            if (isEmpty(vm.editableDonation.paymentType)) { 
                vm.errors.push(gettextCatalog.getString('Please select a payment method.'));
            }

            if (isInvalid || vm.errors.length > 0) {
                vm.showErrors = true;
                return false;
            }
            if (vm.editableDonation.id == 0) {
                vm.isBusy = true;

                //this check is done to tokenize the bank information
                if (vm.editableDonation.paymentType == "B" && vm.editableDonation.authorizationId == undefined) {
                    var dataToTokenize = vm.editableDonation.bwBankNumber + "|"
                        + vm.editableDonation.bwTransitNumber + "|"
                        + vm.editableDonation.bwAccountNumber;

                    vm.editableDonation.bwAccountNumberEncrypted = dataToTokenize;
                    sponsor(lockedBy);
                }
                else {
                    vm.savedaccount = vm.editableDonation.bwAccountNumber + "|" + vm.editableDonation.bwAccountNumberMasked;
                    vm.editableDonation.bwAccountNumber = ""; // blank out original
                    vm.editableDonation.bwAccountNumberMasked = "";
                    sponsor(lockedBy);
                }
            }
            else {
                // this should not happen in this scenario as we are only adding new donations / not editing them
                vm.sponsorship = angular.copy(vm.editableDonation);


            }
        };

        vm.ParsedPhone = function (phone) {
            var parsed = parsePhone(phone);
            if (parsed != undefined && parsed != null) {
                if (!parseInt(parsed.areaCode) > 199) {
                    return false;
                }
            }
            return true;
        }


        //vm.newCreditCard = newCreditCard;
        vm.newCreditCard = newCreditCardMonerisToken;

        function newCreditCardMonerisToken() {
            vm.isMonerisError = false;

            vm.selectedPaymentMethod = {};
            vm.editableDonation.paymentType = "NT";

            vm.editableDonation.ccinfo = {};
            vm.editableDonation.authorizationId = undefined;
            vm.editableDonation.creditCardNumber = '';    // the datakey|ccmask
            vm.editableDonation.expiryMonth = 0;
            vm.editableDonation.expiryYear = 0;
            vm.editableDonation.creditCardWithdrawalDay = 0;
            vm.editableDonation.creditCardDeductionDate = undefined;

            var dt = new Date();
            vm.ccDeductionDate = dt;
            vm.ccWithdrawalDay = dt.getDate();

        }

        vm.monerisSubmit = monerisSubmit;
        vm.monerisUrl = _API_URL._MONERIS_URL;
        vm.monerisUrlSrc = $sce.trustAsResourceUrl(vm.monerisUrl + "?id=" + _API_URL._MONERIS_TOKEN_ID +
                "&css_textbox=border-width:2px;&css_textbox_pan=width:150px" +
                "&enable_exp=1&css_textbox_exp=width:40px;" +
                "&display_labels=1&exp_label=Expiry Date MMYY");

        function monerisSubmit() {
            var monFrameRef = document.getElementById('monerisFrame').contentWindow;
            monFrameRef.postMessage('', vm.monerisUrl);
            return false;
        }

        $scope.$on('monerisReply', function (a, b) {
            if (b.responseCode[0] === "001") {
                // valid stuff!
                // call donorservice to make the datakey permanent
                //debugger;
                donorService.postDatakeyPermanent(b.dataKey).then(
                    function (data) {
                        // success
                        console.log("postDatakeyPermanent Success data:", data);
                        if (data.isSuccess) {
                            var d = new Date();
                            var day = d.getDate();
                            //check those days as this logic exists in dcss to create authorization
                            day = ((day >= 29) ? 1 : day);

                            vm.ccinfo = {
                                type: data.creditCardType,
                                mask: data.maskedPan,
                                //withdrawalDay: vm.ccWithdrawalDay //this is for editable
                                withdrawalDay: day
                            };

                            vm.editableDonation.creditCardNumber = data.datakey + "|" + data.maskedPan;    // the datakey|ccmask
                            vm.editableDonation.expiryMonth = data.expiryYYMM.slice(2);
                            var currentdate = new Date();
                            var currectyear = currentdate.getFullYear() + "";
                            //make the cc expiry year full year string
                            vm.editableDonation.expiryYear = currectyear.substring(0, currectyear.length - 2) + data.expiryYYMM.substring(0, 2);
                            vm.editableDonation.paymentType = "C";
                            //vm.editableDonation.creditCardWithdrawalDay = vm.ccWithdrawalDay; this is for editable
                            vm.editableDonation.creditCardWithdrawalDay = vm.ccinfo.withdrawalDay;
                            vm.editableDonation.creditCardDeductionDate = vm.ccDeductionDate;
                            //vm.selectedPaymentMethod.paymentMethodId = '';
                            vm.newPaymentMethod = true;
                        }
                        else {
                            vm.isMonerisError = true;
                            vm.monerisErrorMessage = data.message;
                        }
                    }, function (data, status) {
                        // fail
                        console.log("postDatakeyPermanent Fail:", data, status);
                        vm.isMonerisError = true;
                        vm.monerisErrorMessage = data.message;
                    }
                );
            }
            else {
                vm.isMonerisError = true;
                vm.monerisErrorMessage = b.errorMessage;
                $scope.$apply();
            }
            if (vm.isMonerisError) {
                vm.errors = vm.monerisErrorMessage;
            }
        })

        function replaceAll(str, find, replace) {
            return str.replace(new RegExp(find, 'g'), replace);
        }

        vm.fillFormArray = function (donorInfo) {
            //debugger;
            for (var i = 0 ; i < vm.titles.length ; i++) {
                if (vm.titles[i]["value"] == donorInfo[0]) {
                    vm.editableDonation.person.title = vm.titles[i]["value"];
                }
            }

            vm.editableDonation.person.firstName = donorInfo[1];
            vm.editableDonation.person.initial = donorInfo[2];
            vm.editableDonation.person.lastName = donorInfo[3];
            vm.editableDonation.person.email = donorInfo[4];
            vm.editableDonation.person.homePhone = donorInfo[5];
            vm.editableDonation.person.businessPhone = donorInfo[6];
            vm.editableDonation.person.businessPhoneExt = donorInfo[7];
            vm.editableDonation.person.mobilePhone = donorInfo[8];
            vm.editableDonation.person.addressLine1 = donorInfo[9];
            vm.editableDonation.person.addressLine2 = donorInfo[10];
            vm.editableDonation.person.city = donorInfo[11];
            vm.editableDonation.person.province = donorInfo[12];
            vm.editableDonation.person.postalCode = donorInfo[13];
            vm.editableDonation.person.subscribeToEmail = (donorInfo[14] == "true" ? true : false);
            vm.editableDonation.sieInOpt = (donorInfo[15] == "true" ? true : false);
        };

        vm.fillForm = function (donorInfo) {
            //debugger;
            vm.editableDonation.person.title = null;
            for (var i = 0 ; i < vm.titles.length ; i++) {
                if (vm.titles[i]["text"] == donorInfo.title) {
                    vm.editableDonation.person.title = vm.titles[i]["value"];
                }
            }

            vm.editableDonation.person.firstName = donorInfo.firstName;
            vm.editableDonation.person.lastName = donorInfo.lastName;
            vm.editableDonation.person.initial = donorInfo.middleInitial;
            vm.editableDonation.person.email = donorInfo.emailAddress;
            vm.editableDonation.person.homePhone = (donorInfo.homePhone != null ? donorInfo.homePhone : "");
            vm.editableDonation.person.addressLine1 = (donorInfo.unit != null ? donorInfo.unit + "-" : "") +
                (donorInfo.streetNumber != null ? donorInfo.streetNumber + " " : "") +
                (donorInfo.streetName != null ? donorInfo.streetName + " " : "") +
                (donorInfo.streetType != null ? donorInfo.streetType + " " : "") +
                (donorInfo.streetDirection != null ? donorInfo.streetDirection : "");
            vm.editableDonation.person.addressLine2 = (donorInfo.deliveryInformation != null ?
                                                        donorInfo.deliveryInformation :
                                                        (donorInfo.additionalAddressInfo != null ? donorInfo.additionalAddressInfo : undefined));
            vm.editableDonation.person.city = donorInfo.city;
            vm.editableDonation.person.province = donorInfo.provinceOrState;
            vm.editableDonation.person.postalCode = donorInfo.postalCodeOrZip;
            vm.editableDonation.person.businessPhone = (donorInfo.businessPhone != null ? donorInfo.businessPhone : "");
            vm.editableDonation.person.businessPhoneExt = (donorInfo.businessPhoneExt != null ? donorInfo.businessPhoneExt : "");
            vm.editableDonation.person.orgName = (donorInfo.organizationName != null ? donorInfo.organizationName : "");
            vm.editableDonation.person.mobilePhone = (donorInfo.cellPhone != null ? donorInfo.cellPhone : "");
            //other info and CC info should go here
        };

        vm.resetForm = function () {
            //get donor information
            if (vm.donorid != "" && vm.donorid != undefined) {
                var encryped = sponsorshipService.encrypt(vm.donorid);
                donorService.getDonor(encryped)
                    .then(function (response) {
                        // success
                        vm.fillForm(response);
                    },
                    function (response) {
                        // error
                    })
                    .then(function () {
                    });
            }
            vm.showErrors = false;
        };

        vm.logForm = function () {

            var sponsoredChildren = [];
            sponsoredChildren.push(
                { projectId: vm.children[0].projectId, childNumber: vm.children[0].childNumber });

            var logObject = {
                status: false,
                person: {
                    firstName: vm.editableDonation.person.firstName,
                    lastName: vm.editableDonation.person.lastName,
                    email: vm.editableDonation.person.email
                },
                sponsoredChildren: sponsoredChildren
            };

            sponsorshipService.logActivity(logObject)
                .then(function (result) {

                    var modalInstance = $uibModal.open({
                        template: '<h1>Log accepted.</h1>'
                    });
                },
                function () {
                    // error
                    vm.errors = sponsorshipService.serviceErrors;
                })
                .then(function () {
                    vm.isBusy = false;
                });
        };

        vm.selectPledgeOffer = function () {
            for (var i = 0; i < vm.pledgeOffers.length; i++) {
                if (vm.pledgeOffers[i].business_Pledge_Description == vm.selectedPOBusinessDescription) {
                    setSelectedPledgeOfferWithSg(vm.pledgeOffers[i]);
                    break;
                }
            }
            vm.selectPaymentFrequency();
        };

        vm.selectPaymentFrequency = function () {
            if (!checkPaymentFrequency()) {

                getPaymentMethodDropdown(vm.selectedPledgeOffer.pledge_Type_Code, vm.selectedDollarHandle.payment_Frequency_Code);
                vm.selectedDollarHandle.dollar_Handle_Custom = vm.selectedDollarHandle.dollar_Handle_Custom == 0
                    ? '' : vm.selectedDollarHandle.dollar_Handle_Custom;

                //todo to ensure all payment_freq are the same when more than 1 selected pleges
                //if (vm.prevPaymentFrequencyCode == '') {
                //    vm.prevPaymentFrequencyCode = vm.selectedDollarHandle.payment_Frequency_Code;
                //}
            }
        }

        vm.selectSg = function () {
            setSelectedPledgeOffer(vm.selectedSgDesignation);
        }

        vm.addMorePledgeOffer = function () {
            vm.findAnotherChild('', '', '');
            vm.showInformationForm = false;
            vm.showSummary = false;
        }

        vm.removePledgeOffer = function (pledgeOffer) {
            var match = false;
            console.log('vm.po=' + JSON.stringify(vm.pledgeOffers));
            for (var i = 0; i < vm.savedPledges.length; i++) {
                if (vm.savedPledges[i].pledge === pledgeOffer.pledge) {
                    //reset custom amount
                    for (var j = 0; j < vm.pledgeOffers.length; j++) {
                        if (vm.pledgeOffers[j].designation_Code === pledgeOffer.pledge.designation_Code) {
                            setSelectedPledgeOfferWithSg(vm.pledgeOffers[j]);
                            match = true;
                        }
                        if (match) {
                            break;
                        }
                    }
                    vm.savedPledges.splice(i, 1);
                    break;
                }
            }
            vm.showSummary = vm.savedPledges.length > 0 || vm.selectedChildren.length > 0;
            vm.showInformationForm = vm.savedPledges.length > 0 || vm.selectedChildren.length > 0;
        }
        
        vm.savePledgeOffer = function (form) {
            resetErrors();
            if (vm.isUndefinedOrNull(vm.selectedDollarHandle.dollar_Handle_Custom)) {
                vm.errorAmountMessage = gettextCatalog.getString("Please enter an amount");
                return false;
            }

            if (!checkPaymentFrequency()) {
                var description = vm.selectedPledgeOffer.pledge_Type_Code != 'SG'
                                ? vm.selectedPledgeOffer.business_Pledge_Description
                                : 'SG - ' + vm.selectedSgDesignation.designation_Description;
                // todo verify payment frequency. if diff payment freq, display err

                form.$show();

                vm.savedPledges.push({
                    pledge: vm.selectedPledgeOffer,
                    dollarHandle: vm.selectedDollarHandle,
                    pledgeTypeCode: vm.selectedDollarHandle.payment_Frequency_Code == 'O' ? 'SG' : vm.selectedPledgeOffer.pledge_Type_Code,
                    product: description,
                    formTitle: description
                });

                console.log('vm.savedPledges', vm.savedPledges);

                vm.overallPaymentFreq = vm.savedPledges[0].dollarHandle.payment_Frequency_Description;
                vm.showInformationForm = true;
                vm.showSummary = true;
                if (vm.prevPaymentFrequencyCode == '') {
                    vm.prevPaymentFrequencyCode = vm.selectedDollarHandle.payment_Frequency_Code;
                }
            }
        }


        // ====================== private functions ==========================================

        function sponsor(lockedBy) {
            // add the current child to the "list"
            vm.editableDonation.sponsorshipDonations = [];
            for (var i = 0; i < vm.selectedChildren.length; i++) {
                vm.editableDonation.sponsorshipDonations.push({
                    projectId: vm.selectedChildren[i].child.projectId,
                    childNumber: vm.selectedChildren[i].child.childNumber,
                    amount: vm.spAmount,
                    motivationId: vm.motcode,
                    designationCode: vm.spDescode
                });
            }

            vm.editableDonation.regularDonations = [];
            for (var i = 0; i < vm.savedPledges.length; i++) {
                vm.editableDonation.regularDonations.push({
                    amount: vm.savedPledges[i].dollarHandle.dollar_Handle_Custom,
                    motivationId: vm.motcode,
                    designationCode: vm.savedPledges[i].pledge.designation_Code,
                    product: vm.savedPledges[i].product,
                    formTitle: vm.savedPledges[i].formTitle,
                    pledgeType: vm.savedPledges[i].dollarHandle.payment_Frequency_Code == 'O' ? 'SG' : getPledgeTypeCode(vm.savedPledges[i].pledge.designation_Code),
                    monthlyPledgeRecurrence: 0,
                });
            }

            // addon obsolete
            for (var i = 0; i < vm.addonPledges.length; i++) {
                var pledge = vm.criteriaMatch(vm.pledges, vm.addonPledges[i].designation);
                vm.editableDonation.donations.push({
                    amount: vm.addonPledges[i].amount,
                    designationCode: vm.addonPledges[i].designation == 0 ? vm.addonPledges[i].editdesignation : vm.addonPledges[i].designation,
                    pledgeType: pledge[0].type,
                    formTitle: pledge[0].name,
                    monthlyPledgeRecurrence: 0,
                    product: pledge[0].name
                });
            }

            if (vm.editableDonation.paymentType == "C"){
                if (vm.editableDonation.authorizationId == undefined) {
                    vm.editableDonation.creditCardNumberEncrypted = vm.editableDonation.creditCardNumber;
                    vm.editableDonation.creditCardNumber = ""; // blank out original
                }
                //vm.editableDonation.creditCardWithdrawalDay = vm.ccWithdrawalDay; // this is for editable
                vm.editableDonation.creditCardWithdrawalDay = vm.ccinfo.withdrawalDay;
                vm.editableDonation.creditCardDeductionDate = vm.ccDeductionDate;
            }

            if (vm.motcode != "") {
                vm.editableDonation.motivationId = vm.motcode;
            }

            if (vm.lang != "") {
                vm.editableDonation.lang = vm.lang.toUpperCase();
                vm.editableDonation.person.lang = vm.lang.toUpperCase();
            }

            //default SP only
            vm.editableDonation.product = _API_URL._SPONSORSHIP_TM;
            vm.editableDonation.formTitle = _API_URL._SPONSORSHIP_TM;
            vm.editableDonation.objectType = _API_URL._SPONSORSHIP_TM;
            vm.editableDonation.pledgeType = 'SP';
            vm.editableDonation.designationCode = vm.spDescode;

            if (vm.selectedChildren.length == 0 && vm.savedPledges.length > 0) {
                vm.editableDonation.product = _API_URL._DONATION_TM;
                vm.editableDonation.formTitle = _API_URL._DONATION_TM;
                vm.editableDonation.objectType = _API_URL._DONATION_TM;
                vm.editableDonation.pledgeType = vm.editableDonation.regularDonations[0].pledgeType;
                vm.editableDonation.designationCode = vm.savedPledges[0].pledge.designation_Code;
            }
            vm.editableDonation.amount = vm.totalAmounts();

            vm.editableDonation.person.homePhone = replaceAll(vm.editableDonation.person.homePhone, '-', '');
            vm.editableDonation.person.businessPhone = replaceAll(vm.editableDonation.person.businessPhone, '-', '');
            vm.editableDonation.person.mobilePhone = replaceAll(vm.editableDonation.person.mobilePhone, '-', '');

            vm.isBusy = true;
            console.log('sponsor, vm.editableDonation' + JSON.stringify(vm.editableDonation));

            var pledgeDescription = '';
            if (vm.savedPledges.length > 0) {
                pledgeDescription = vm.savedPledges[0].dollarHandle.payment_Frequency_Description;
            }

            vm.errors = [];
            donationService.insertDonation(vm.editableDonation, lockedBy)
                .then(function (result) {
                    if (vm.editableDonation.person.email != "" && vm.editableDonation.person.email != null 
                            && vm.editableDonation.person.email != undefined) {
                        donationService.sendEmail(result.id)
                        .then(function (data) {
                            var modalInstance = $uibModal.open({
                                animation: true,
                                templateUrl: 'modalThankYou.html',
                                controller: 'ModalController',
                                resolve: {
                                    modalContent: function () {
                                        return {
                                            children: vm.selectedChildren,
                                            donorId: vm.donorid,
                                            donations: vm.savedPledges,
                                            pledgeDescription: pledgeDescription,
                                            totalSpAmounts: vm.totalSpAmounts(),
                                            totalNonSpAmounts: vm.totalNonSpAmounts(),
                                            totalAmounts: vm.totalAmounts()
                                        };
                                    }
                                },
                                backdrop: 'static'
                            });

                            modalInstance.result.then(function () { }, function () { })
                                .then(function () {
                                    $window.location.reload();
                                    vm.isBusy = false;
                                });
                        },
                            function (response) {
                                // error
                                vm.errors = response;
                                vm.isBusy = false;
                                if (vm.editableDonation.paymentType == "B" && vm.editableDonation.authorizationId != undefined && vm.savedaccount != '') {
                                    var accounts = vm.savedaccount.split('|');
                                    vm.editableDonation.bwAccountNumber = accounts[0]; // blank out original
                                    vm.editableDonation.bwAccountNumberMasked = accounts[1];
                                }
                            })
                            .then(function () {
                                //vm.isBusy = false;
                            });
                    }
                    else {
                        var modalInstance = $uibModal.open({
                            animation: true,
                            templateUrl: 'modalThankYou.html',
                            controller: 'ModalController',
                            resolve: {
                                modalContent: function () {
                                    return {
                                        children: vm.selectedChildren,
                                        donorId: vm.donorid,
                                        donations: vm.savedPledges,
                                        pledgeDescription: pledgeDescription,
                                        totalSpAmounts: vm.totalSpAmounts(),
                                        totalNonSpAmounts: vm.totalNonSpAmounts(),
                                        totalAmounts: vm.totalAmounts()
                                    };
                                }
                            },
                            backdrop: 'static'
                        });

                        modalInstance.result.then(function () { }, function () { })
                            .then(function () {
                                $window.location.reload();
                                vm.isBusy = false;
                            });
                    }
                },
                function (response) {
                    //alert('err, data=' +response);
                    // error
                    vm.errors.push('Transaction is not saved. ' + response);
                    vm.isBusy = false;
                    if (vm.editableDonation.paymentType == "B" && vm.editableDonation.authorizationId != undefined && vm.savedaccount != '') {
                        var accounts = vm.savedaccount.split('|');
                        vm.editableDonation.bwAccountNumber = accounts[0]; // blank out original
                        vm.editableDonation.bwAccountNumberMasked = accounts[1];
                    }
                })
                .then(function () {
                    //vm.isBusy = false;
                });
        }

        function initializePaymentMethods() {

            var index;
            if (vm.allPaymentMethods != null && vm.allPaymentMethods != undefined) {
                for (index = 0; index < vm.allPaymentMethods.length; ++index) {

                    vm.allPaymentMethods[index].canAcceptPledges = false;
                    if (vm.allPaymentMethods[index].authorizationType == "C") {
                        var expiryDate = new Date(
                                vm.allPaymentMethods[index].creditCardExpiryYear,
                                vm.allPaymentMethods[index].creditCardExpiryMonth, 1);
                        var currentDate = new Date();

                        if (expiryDate > currentDate) {
                            vm.allPaymentMethods[index].canAcceptPledges = true;
                        }

                        if (vm.allPaymentMethods[index].creditCardType == "A") {
                            vm.allPaymentMethods[index].paymentMethodName = "American Express: " + vm.allPaymentMethods[index].paymentMethodName;
                        }
                        if (vm.allPaymentMethods[index].creditCardType == "M") {
                            vm.allPaymentMethods[index].paymentMethodName = "Mastercard: " + vm.allPaymentMethods[index].paymentMethodName;
                        }
                        if (vm.allPaymentMethods[index].creditCardType == "V") {
                            vm.allPaymentMethods[index].paymentMethodName = "Visa: " + vm.allPaymentMethods[index].paymentMethodName;
                        }
                        vm.paymentMethodsWithoutBank.push(vm.allPaymentMethods[index]);
                    }
                    if (vm.allPaymentMethods[index].authorizationType == "P") {
                        vm.allPaymentMethods[index].bankAccountName = vm.allPaymentMethods[index].bankAccountName;
                        if (vm.allPaymentMethods[index].paymentMethodName.length > 13) {
                            vm.allPaymentMethods[index].paymentMethodName = vm.allPaymentMethods[index].paymentMethodName.substring(0, 10) + "***" +
                                    vm.allPaymentMethods[index].paymentMethodName.substring(13);
                        }
                        else {
                            vm.allPaymentMethods[index].paymentMethodName = "***" + vm.allPaymentMethods[index].paymentMethodName.substring(3);
                        }
                        vm.allPaymentMethods[index].canAcceptPledges = true;
                    }

                    //select default the first - ordering done in service
                    if (vm.payment == "") {
                        if (vm.allPaymentMethods[index].canAcceptPledges && (vm.selectedPaymentMethod.authorizationType == undefined || vm.selectedPaymentMethod.authorizationType == null || vm.selectedPaymentMethod.authorizationType == "")) {
                            vm.selectPayment(vm.allPaymentMethods[index].paymentMethodId);
                        }
                    }
                }
            }
            vm.paymentMethods = vm.allPaymentMethods;
        }

        function padLeft(nr, n, str) {
            return Array(n - String(nr).length + 1).join(str || '0') + nr;
        }

        function searchChildren() {

            var l = vm.lang;
            vm.errors = [];
            if (vm.lang == 'fr') {
                l = (vm.includeEnglish && vm.includeFrench ? "en|fr" : (vm.includeEnglish ? "en" : "fr"));
            }
            else if (vm.lang == 'zh') {
                l = "en|zh";
            }

            var url = sponsorshipService.serviceUrl + "?pageSize=1&page=0&lang=" + vm.lang + "&withdropdowninfo=true&offset=" + vm.offset;

            if (vm.searchParams.country != "-") {
                url = url + "&country=" + vm.searchParams.country;
            }
            if (vm.searchParams.gender != "-") {
                url = url + "&gender=" + vm.searchParams.gender;
            }
            if (vm.searchParams.month != -1) {
                url = url + "&month=" + vm.searchParams.month;
            }
            if (vm.searchParams.day != -1) {
                url = url + "&day=" + vm.searchParams.day;
            }
            if (vm.searchParams.age != -1) {
                url = url + "&agemin=" + vm.searchParams.age;
                if (vm.searchParams.age == 12) {
                    url = url + "&agemax=99";
                }
                else {
                    url = url + "&agemax=" + vm.searchParams.age;
                }
            }
            if (vm.ignoreMotivation == false && vm.motcode != "" && vm.motcode != undefined) {
                url = url + "&motcode=" + vm.motcode;
            }

            url = url + "&langPool=" + l;

            vm.isBusy = true;
            sponsorshipService.searchSpecificUrl(url)
                .then(function (data) {
                    // success
                    vm.totalCount = angular.copy(data.totalCount);
                    vm.totalPages = angular.copy(data.totalPages);
                    vm.currentPage = angular.copy(data.currentPage);
                    vm.children = angular.copy(data.results);
                    vm.prevPageUrl = angular.copy(data.prevPageUrl);
                    vm.nextPageUrl = angular.copy(data.nextPageUrl);

                    vm.genderCodeList = angular.copy(data.genderCodeList);
                    vm.countryCodeList = angular.copy(data.countryCodeList);
                    vm.monthList = angular.copy(data.monthList);
                    vm.dayList = angular.copy(data.dayList);
                    vm.ageList = angular.copy(data.ageList);

                    fixDropdowns();

                    //now get the selected children 
                    getSelectedChildren();
                },
                function () {
                    // error
                })
                .then(function () {
                    if (!vm.isInitalLoad) {
                        vm.isBusy = false;
                    }

                });
        }

        function selectPayment(paymentMethodId) {
            // grab the paymentMethodDetails
            if (paymentMethodId == vm.newCreditCardMethodId) {
                return;
            }
            var paymentMethodIndex;
            for (paymentMethodIndex = 0; paymentMethodIndex < vm.allPaymentMethods.length; ++paymentMethodIndex) {
                if (vm.allPaymentMethods[paymentMethodIndex].paymentMethodId == paymentMethodId) {
                    vm.selectedPaymentMethod = vm.allPaymentMethods[paymentMethodIndex];
                    break;
                }
            }
            vm.editableDonation.paymentType = vm.selectedPaymentMethod.authorizationType;
            //set the authorizationId as dcssId if needed
            vm.editableDonation.authorizationId = (vm.selectedPaymentMethod.authorizationId == null || vm.selectedPaymentMethod.authorizationId == "" ? vm.selectedPaymentMethod.dcssCreditCardId : vm.selectedPaymentMethod.authorizationId);
            if (vm.selectedPaymentMethod.authorizationType == "C") {
                var ccwDate = new Date(vm.selectedPaymentMethod.nextDebitDate.replace(/-/g, "/").replace("T", " "));

                var d = new Date();
                var day = d.getDate();
                //check those days as this logic exists in dcss to create authorization
                day = ((day >= 29) ? 1 : day);

                vm.ccinfo = {
                    type: vm.selectedPaymentMethod.creditCardType,
                    mask: vm.selectedPaymentMethod.creditCardNumberMasked,
                    withdrawalDay: (vm.selectedPaymentMethod.frequency == "M" ? ((ccwDate != NaN && ccwDate != undefined) ? ccwDate.getDate() : "") : day),
                    frequency: vm.selectedPaymentMethod.frequency == "M" ? "" : " / " + vm.selectedPaymentMethod.frequency
                };
                vm.editableDonation.creditCardNumber = undefined;
                vm.editableDonation.expiryMonth = vm.selectedPaymentMethod.creditCardExpiryMonth;
                vm.editableDonation.expiryYear = vm.selectedPaymentMethod.creditCardExpiryYear;
                //vm.editableDonation.creditCardWithdrawalDay = vm.ccWithdrawalDay; // this is for editable
                vm.editableDonation.creditCardWithdrawalDay = vm.ccinfo.withdrawalDay;
                vm.editableDonation.creditCardDeductionDate = vm.ccDeductionDate;

                vm.editableDonation.bwAccountHolder = undefined;
                vm.editableDonation.bwAccountNumber = undefined;
                vm.editableDonation.bwAccountNumberMasked = undefined;
                vm.editableDonation.bwBankNumber = undefined;
                vm.editableDonation.bwTransitNumber = undefined;
            }

            if (vm.selectedPaymentMethod.authorizationType == "P") {

                var bwDate = new Date(vm.selectedPaymentMethod.nextDebitDate.replace(/-/g, "/").replace("T", " "));
                if (bwDate != NaN && bwDate != undefined) {
                    vm.editableDonation.bwDay = bwDate.getDate();
                }

                vm.editableDonation.paymentType = "B";
                if (!vm.editableDonation.bwDay) {
                    vm.editableDonation.bwDay = 1;
                }

                vm.editableDonation.bwAccountHolder = vm.selectedPaymentMethod.bankAccountName;
                vm.editableDonation.bwAccountNumber = vm.selectedPaymentMethod.bankAccountNumber;
                vm.editableDonation.bwAccountNumberMasked = "***" + vm.selectedPaymentMethod.bankAccountNumber.substring(3);
                vm.editableDonation.bwBankNumber = vm.selectedPaymentMethod.bankNumber;
                vm.editableDonation.bwTransitNumber = vm.selectedPaymentMethod.bankTransitNumber;

                vm.editableDonation.creditCardNumber = undefined;
                vm.editableDonation.expiryMonth = undefined;
                vm.editableDonation.expiryYear = undefined;
                vm.editableDonation.creditCardWithdrawalDay = undefined;
                vm.editableDonation.creditCardDeductionDate = undefined;
                vm.ccinfo = { type: "", mask: "", withdrawalDay: "", frequency: "" };
            }

        }

        function selectPaymentMethod() {
            return (vm.selectedPaymentMethod.paymentMethodName != undefined ?
                "Selected " + vm.selectedPaymentMethod.paymentMethodName :
                (vm.editableDonation.paymentType == "B" ? "Selected New Bank Account" :
                ((vm.editableDonation.paymentType == "C" || vm.editableDonation.paymentType == "NT") ? "Selected New Credit Card" :
                (vm.editableDonation.paymentType == "Q" ? "Selected Cheque" : "Please select payment"))))
        }

        function newBankAccount() {
            vm.selectedPaymentMethod = {};

            vm.editableDonation.paymentType = "B";
            vm.editableDonation.bwAccountHolder = vm.editableDonation.person.firstName + " " + vm.editableDonation.person.lastName;
            vm.editableDonation.bwAccountNumber = undefined;
            vm.editableDonation.bwAccountNumberMasked = undefined;
            vm.editableDonation.bwBankNumber = undefined;
            vm.editableDonation.bwTransitNumber = undefined;
            vm.editableDonation.bwDay = 1;
            vm.editableDonation.authorizationId = undefined;
        }

        function selectCheque() {
            vm.selectedPaymentMethod = {};

            vm.editableDonation.paymentType = "Q";
            vm.editableDonation.bwAccountHolder = undefined;
            vm.editableDonation.bwAccountNumber = undefined;
            vm.editableDonation.bwAccountNumberMasked = undefined;
            vm.editableDonation.bwBankNumber = undefined;
            vm.editableDonation.bwTransitNumber = undefined;
            vm.editableDonation.creditCardNumber = undefined;
            vm.editableDonation.expiryMonth = undefined;
            vm.editableDonation.expiryYear = undefined;
            vm.editableDonation.creditCardWithdrawalDay = undefined;
            vm.editableDonation.creditCardDeductionDate = undefined;
            vm.ccinfo = { type: "", mask: "", withdrawalDay: "", frequency: "" };
            vm.editableDonation.authorizationId = undefined;

        }

        function getSelectedChildren() {
            if (vm.TxChildren != undefined && vm.TxChildren != "") {
                //parse
                var childIds = vm.TxChildren.split("|");
                if (childIds.length > 0) {
                    for (var i = 0; i < childIds.length; i++) {
                        //get child
                        vm.isBusy = true;
                        sponsorshipService.getChildInformation(childIds[i])
                        .then(function (result) {
                            // success
                            vm.selectedChildren.push({ child: angular.copy(result) });
                        },
                        function () {
                            // error
                            vm.serviceAvailable = false;
                            vm.isBusy = false;
                        })
                        .then(function () {
                        });
                    }
                    vm.showInformationForm = true;
                }
            }
            else {
                vm.serviceAvailable = true;
            }
        }

        function getSelectedPledges() {
            if (vm.TxPledges != undefined && vm.TxPledges != "") {
                //parse
                var pledgeId = vm.TxPledges.split("|");
                if (pledgeId.length > 0) {
                    for (var i = 0; i < pledgeId.length; i++) {
                        var items = pledgeId[i].split('-');
                        vm.addExistingPledge(parseInt(items[0]), parseInt(items[1]), parseInt(items[2]));
                    }
                }
                vm.addonDonation = true;
            }
        }

        function fixDropdowns() {
            // adjust the agelist
            var twelvePlus = false;
            var arrayLength = vm.ageList.length;
            for (var i = arrayLength - 1; i >= 0; i--) {
                if (vm.ageList[i].value >= 12) {
                    twelvePlus = true;
                    vm.ageList.splice(i, 1);
                }
                else {
                    break;
                }
            }
            if (twelvePlus) {
                vm.ageList.push({ "value": 12, "description": "12+" });
            }

            // ----------------------------------------------
            vm.genderCodeList.unshift({ "code": "-", "description": gettextCatalog.getString("Any Gender") });
            vm.ageList.unshift({ "value": -1, "description": gettextCatalog.getString("Any Age") });
            vm.monthList.unshift({ "value": -1, "description": gettextCatalog.getString("Any Month") });
            vm.dayList.unshift({ "value": -1, "description": gettextCatalog.getString("Any Birthday") });
            vm.countryCodeList.unshift({ "code": "-", "description": gettextCatalog.getString("Any Country") });
        }

        function resetSearchParameters() {
            vm.searchParams = {
                gender: "-",
                age: -1,
                maxage: -1,
                month: -1,
                day: -1,
                country: "-"
            };
            vm.ignoreMotivation = false;
        };

        function populateCCInfo() {
            //debugger;
            if (vm.payment != "" && vm.payment != null && vm.payment != undefined) {
                // this should nolonger be populated as this was coming from tokenex
                //fill payment information from the parameters
                var cc = vm.payment.split("||");
                var firstfour = cc[4].substring(0, 4);
                var lastfour = cc[4].slice(-4);

                var d = new Date();
                var day = d.getDate();
                //check those days as this logic exists in dcss to create authorization
                day = ((day >= 29) ? 1 : day);
                vm.ccinfo = { type: cc[0], mask: firstfour + "***" + lastfour, withdrawalDay: day };
                vm.editableDonation.creditCardNumber = cc[1];    // the datakey
                vm.editableDonation.expiryMonth = cc[2];
                var currentdate = new Date();
                var currectyear = currentdate.getFullYear() + "";
                //make the cc expiry year full year string
                vm.editableDonation.expiryYear = (cc[3].length == 2 ? currectyear.substring(0, currectyear.length - 2) + cc[3] : cc[3]);
                vm.editableDonation.creditCardWithdrawalDay = day;
                vm.editableDonation.creditCardDeductionDate = undefined;

            }
        };

        function getDonorType() {
            if (vm.editableDonation != undefined && vm.editableDonation.person != undefined &&
                    vm.editableDonation.person.orgName != null && vm.editableDonation.person.orgName != '') {
                return gettextCatalog.getString("Group/Business");
            }
            return gettextCatalog.getString("Individual");
        }

        function isUndefinedOrNull(val) {
            return angular.isUndefined(val) || val === null || val == '';
        }
    
        function isEmpty(obj) {
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop))
                    return false;
            }
            return true;
        }

        function checkPaymentFrequency() {
            var error = false;
            vm.errorPaymentFreq = false;

            // ensure all transactions are the same payment frequency
            //if (vm.prevPaymentFrequencyCode != '' && vm.prevPaymentFrequencyCode != vm.selectedDollarHandle.payment_Frequency_Code) {
            //    vm.errorPaymentFreqMessage = gettextCatalog.getString("Your selected payment method is " + vm.savedPledges[0].paymentFrequencyDesc + ". You can't choose another payment frequency.");
            //    error = true;
            //}
            return error;
        }

        function getPaymentFrequencyDropDown(pledgeTypeCode, desCode) {
            for (var i = 0; i < vm.pledgeOffers.length; i++) {
                if (vm.pledgeOffers[i].pledge_Type_Code == pledgeTypeCode && vm.pledgeOffers[i].designation_Code == desCode) {
                    return vm.pledgeOffers[i].pledgeOffersDollarHandles;
                }
            }
            return null;
        }
        
        function getPaymentMethodDropdown(pledgeTypeCode, paymentFreq) {
            // if sg, remove bank from dropdown
            vm.paymentMethods = vm.allPaymentMethods;
            var firstAvailable = '';
            if (pledgeTypeCode.toUpperCase() == 'SG' || paymentFreq == 'O') {
                vm.paymentMethods = vm.paymentMethodsWithoutBank;
            }
            // if the dropdown is empty, reset selectedPaymentMethod
            if (vm.paymentMethods == '' || vm.paymentMethods == null) {
                vm.selectedPaymentMethod = '';
            }
            else {
                var found = false;
                for (var i = 0; i < vm.paymentMethods.length; i++) {
                    if ((vm.paymentMethods[i] === vm.selectedPaymentMethod)
                            && ((vm.selectedPaymentMethod.authorizationType == 'P' && paymentFreq == 'M')
                            || (vm.selectedPaymentMethod.authorizationType == 'C' && paymentFreq == 'O'))) {
                        found = true;
                        break;
                    }
                    else {
                        if (vm.paymentMethods[i].canAcceptPledges && firstAvailable == '') {
                            firstAvailable = vm.paymentMethods[i];
                        }
                    }
                }
                if (!found) {
                    if (!vm.newPaymentMethod) {
                        vm.selectedPaymentMethod = firstAvailable;
                    }
                    selectPayment(vm.selectedPaymentMethod.paymentMethodId);
                }
            }
        }

        function getPledgeTypeCode(descode) {
            for (var i = 0; i < vm.pledgeOffers.length; i++) {
                if (vm.pledgeOffers[i].designation_Code == descode) {
                    return vm.pledgeOffers[i].pledge_Type_Code;
                }
            }
            return '';
        }

        function setSelectedPledgeOffer(pledgeOffer) {
            vm.selectedPledgeOffer = angular.copy(pledgeOffer);
            vm.selectedDollarHandle = vm.selectedPledgeOffer.pledgeOffersDollarHandles[0];
            vm.selectedDollarHandle.dollar_Handle_Custom = vm.selectedDollarHandle.dollar_Handle_Custom == 0
                ? '' : vm.selectedDollarHandle.dollar_Handle_Custom
        }

        function setSelectedPledgeOfferWithSg(pledgeOffer) {
            setSelectedPledgeOffer(pledgeOffer);
            vm.isSg = vm.selectedPledgeOffer.pledge_Type_Code == 'SG';
            vm.selectedSgDesignation = vm.selectedPledgeOffer.pledge_Type_Code == 'SG' ? pledgeOffer : null;
        }

        function resetErrors() {
            vm.errors = [];
            vm.errorPaymentFreq = false;
            vm.errorAmountMessage = '';
        }

        /**
        * @param fromDate {string} in date format
        * @param numberMonths {number} e.g. 1, 2, 3...
        * @returns {string} in date format
        */
        function addMonths(fromDate, numberMonths) {
            var dateObject = fromDate;
            var day = dateObject.getDate(); // returns day of the month number

            // avoid date calculation errors
            dateObject.setHours(20);

            // add months and set date to last day of the correct month
            dateObject.setMonth(dateObject.getMonth() + numberMonths + 1, 0);

            // set day number to min of either the original one or last day of month
            dateObject.setDate(Math.min(day, dateObject.getDate()));
            return dateObject;
        }

        console.log("vm", vm);
    }
})();
