﻿(function () {
    'use strict';

    angular
        .module('telemarketingApp')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$scope', '$location', '$state', '$stateParams', 'AuthenticationService', 'FlashService', 'gettextCatalog'];
    function LoginController($scope, $location, $state, $stateParams, AuthenticationService, FlashService, gettextCatalog) {
        var vm = this;

        vm.motcode = "";
        vm.donorid = "";
        vm.lang = "en";
        vm.pledgetype = "";
        vm.descode = "";

        vm.login = login;

        $scope.$on("$stateChangeSuccess", function viewParams() {
            vm.motcode = $stateParams.motcode;
            vm.donorid = $stateParams.donorid;
            vm.pledgetype = $stateParams.pledgetype;
            vm.descode = $stateParams.descode;
            vm.lang = (($stateParams.lang == undefined || $stateParams.lang == "") ? "en" : $stateParams.lang);

            gettextCatalog.setCurrentLanguage(vm.lang);
        });

        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();

        function login() {
            vm.dataLoading = true;
            AuthenticationService.Login(vm.username, vm.password, function (response) {
                if (response.success) {
                    AuthenticationService.SetCredentials(vm.username, vm.password);
                    if (vm.donorid != undefined && vm.donorid != "") {
                        //go to the sponsorship page
                        $state.go('sponsor', { donorid: vm.donorid, motcode: vm.motcode, lang: vm.lang, pledgetype: vm.pledgetype, descode: vm.descode });
                    }
                    else {
                        $state.go('sponsorblank');
                    }
                    //$location.path('/sponsor');
                } else {
                    FlashService.Error(response.message);
                    vm.dataLoading = false;
                }
            });
        };
    }

})();