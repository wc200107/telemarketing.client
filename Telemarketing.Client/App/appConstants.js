(function () {
 'use strict';

 angular.module('telemarketingApp')

.constant('_API_URL', {_BASE_DONATION_API_URL:'https://dev-svc.worldvision.ca/DonationService/api/',_BASE_DONOR_API_URL:'https://dev-svc.worldvision.ca/DonorService/api/',_BASE_SPONSORSHIP_API_URL:'https://dev-svc.worldvision.ca/sponsorshipservice/api/',_BASE_COMMON_API_URL:'https://dev-svc.worldvision.ca/commonservice/api/',_MONERIS_URL:'https://esqa.moneris.com/HPPtoken/index.php',_MONERIS_TOKEN_ID:'htXCMFHXHGDFMAX',_DONATION_TM:'Sponsorship-TM',_SPONSORSHIP_TM:'Sponsorship-TM',_MIXED_TM:'Sponsorship-TM',_SP_DESCODE:40501,_SHOW_DEDUCTION_DATE:false,_SPADDON:true})

; })();