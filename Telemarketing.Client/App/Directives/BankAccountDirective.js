﻿(function () {
    'use strict';

    angular
        .module('telemarketingApp')
        .directive('bankaccount',
            function () {
                return {
                    restrict: 'A',
                    templateUrl: 'App/Directives/bankaccount.html'
                };
            }
        );
})();