﻿(function () {
    'use strict';

    // copied from http://codepen.io/anon/pen/KFsej
    angular
        .module('telemarketingApp')
        .directive('sponsorshipForm',
        function () {
            return {
                restrict: 'E',
                templateUrl: 'App/Dashboard/SponsorshipTemplate.html'
            }
        })
        .directive('nonSpForm',
        function () {
            return {
                restrict: 'E',
                templateUrl: 'App/Dashboard/SponsorshipTemplate.html'
            }
        })
        .directive('validNumber', function () {
            return {
                require: '?ngModel',
                link: function (scope, element, attrs, ngModelCtrl) {
                    if (!ngModelCtrl) {
                        return;
                    }

                    ngModelCtrl.$parsers.push(function (val) {
                        var clean = val.replace(/[^0-9]+/g, '');
                        if (val !== clean) {
                            ngModelCtrl.$setViewValue(clean);
                            ngModelCtrl.$render();
                        }
                        return clean;
                    });

                    element.bind('keypress', function (event) {
                        if (event.keyCode === 32) {
                            event.preventDefault();
                        }
                    });
                }
            };
        })
    .directive('creditCardType',
    function () {
        return {
            require: 'ngModel'
            , link: function (scope, elm, attrs, ctrl) {

                var validate = function (value) {
                    scope.ccinfo.type =
                        (/^5[1-5]/.test(value)) ? "Master Card"
                        : (/^4/.test(value)) ? "Visa"
                        : (/^3[47]/.test(value)) ? 'American Express'
                        //: (/^6011|65|64[4-9]|622(1(2[6-9]|[3-9]\d)|[2-8]\d{2}|9([01]\d|2[0-5]))/.test(value)) ? 'discover'
                        : undefined;
                    return !!scope.ccinfo.type
                };

                var validator = function (value) {
                    ctrl.$setValidity('invalid', validate(value));
                    return value;
                };


                ctrl.$parsers.unshift(validator);
                ctrl.$formatters.unshift(validator);


                attrs.$observe('creditCardType', function (value) {
                    console.log("im here " + value);
                    enabled = (value == "true");
                    if (enabled) {
                        console.log("im enabled");
                        ctrl.$setValidity('invalid', validate(ctrl.$viewValue));
                    }
                    else {
                        ctrl.$setValidity('invalid', false);
                    }
                });

                //ctrl.$parsers.unshift(function (value) {
                //    scope.ccinfo.type =
                //        (/^5[1-5]/.test(value)) ? "Master Card"
                //        : (/^4/.test(value)) ? "Visa"
                //        : (/^3[47]/.test(value)) ? 'American Express'
                //        //: (/^6011|65|64[4-9]|622(1(2[6-9]|[3-9]\d)|[2-8]\d{2}|9([01]\d|2[0-5]))/.test(value)) ? 'discover'
                //        : undefined;
                //    if (enabled) {
                //        console.log("enabled:" + value);
                //        ctrl.$setValidity('invalid', !!scope.ccinfo.type);
                //    }
                //    else {
                //        console.log("should be valid");
                //        ctrl.$setValidity('invalid', false);
                //    }
                //    return value;
                //})

            }
        }
    })
    .directive('cardExpiration',
    function () {
        return {
            require: 'ngModel'
            , link: function (scope, elm, attrs, ctrl) {
                scope.$watch('[vm.editableDonation.paymentType, vm.editableDonation.expiryMonth, vm.editableDonation.expiryYear]', function (value) {
                    //ctrl.$setValidity('invalid', true);
                    //debugger;
                    if (scope.vm.editableDonation.paymentType == "C" &&
                            (scope.vm.editableDonation.expiryYear == scope.vm.currentYear
                            && scope.vm.editableDonation.expiryMonth <= scope.vm.currentMonth)) {
                        //console.log("should be invalid: " + scope.editableSponsorship.paymentType + "|" + scope.editableSponsorship.expiryYear + "|" + scope.editableSponsorship.expiryMonth);
                        //ctrl.$setValidity('invalid', false);
                        ctrl.$setValidity('cardExpiration', false);
                    }
                    else {
                        //console.log("should be valid");
                        ctrl.$setValidity('cardExpiration', true);
                    }
                    return value
                }, true)
            }
        }
    })
    .directive('ngMin', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, elem, attr, ctrl) {
                scope.$watch(attr.ngMin, function () {
                    if (ctrl.$isDirty) ctrl.$setViewValue(ctrl.$viewValue);
                });
                var isEmpty = function (value) {
                    return angular.isUndefined(value) || value === "" || value === null;
                }
                var minValidator = function (value) {
                    var min = scope.$eval(attr.ngMin) || 0;
                    if (!isEmpty(value) && value < min) {
                        ctrl.$setValidity('ngMin', false);
                        return undefined;
                    } else {
                        ctrl.$setValidity('ngMin', true);
                        return value;
                    }
                };

                ctrl.$parsers.push(minValidator);
                ctrl.$formatters.push(minValidator);
            }
        };
    })
    .directive('ngMax', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, elem, attr, ctrl) {
                scope.$watch(attr.ngMax, function () {
                    if (ctrl.$isDirty) ctrl.$setViewValue(ctrl.$viewValue);
                });
                var isEmpty = function (value) {
                    return angular.isUndefined(value) || value === "" || value === null;
                }
                var maxValidator = function (value) {
                    var max = scope.$eval(attr.ngMax) || Infinity;
                    if (!isEmpty(value) && value > max) {
                        ctrl.$setValidity('ngMax', false);
                        return undefined;
                    } else {
                        ctrl.$setValidity('ngMax', true);
                        return value;
                    }
                };

                ctrl.$parsers.push(maxValidator);
                ctrl.$formatters.push(maxValidator);
            }
        };
    })
    .filter('range',
    function () {
        var filter = function (arr, lower, upper) {
            for (var i = lower; i <= upper; i++) arr.push(i)
            return arr
        }
        return filter
    })
    .filter('numberFixedLen',
    function () {
        return function (n, len) {
            var num = parseInt(n, 10);
            len = parseInt(len, 10);
            if (isNaN(num) || isNaN(len)) {
                return n;
            }
            num = '' + num;
            while (num.length < len) {
                num = '0' + num;
            }
            return num;
        };
    });
})();

