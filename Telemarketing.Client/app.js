﻿(function () {
    'use strict';

    angular.module('telemarketingApp', ['ui.bootstrap', 'ui.router', 'ngRoute', 'ngCookies', 'gettext', 'xeditable'])
        .config(config)
        .run(run);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {

        var currentdate = Date.now();
        $stateProvider
            .state('home', {
                url: "/",
                controller: "LoginController",
                templateUrl: 'App/Dashboard/LoginView.html?v=' + currentdate,
                controllerAs: 'vm'
            })
            .state('login', {
                url: "/login",
                controller: "LoginController",
                templateUrl: 'App/Dashboard/LoginView.html?v=' + currentdate,
                controllerAs: 'vm'
            })
            .state('loginparam', {
                url: "/login/:donorid/:motcode/:lang/:pledgetype/:descode",
                controller: "LoginController",
                templateUrl: 'App/Dashboard/LoginView.html?v=' + currentdate,
                params: {
                    donorid: null,
                    motcode: null,
                    lang: "en",
                    pledgetype: null,
                    descode: null
                },
                controllerAs: 'vm'
            })
            .state('sponsorselect', {
                url: "/sponsor/:donorid/:motcode/:lang/:pledgetype/:descode",
                controller: 'sponsorshipController',
                templateUrl: 'App/Dashboard/SponsorshipTemplate.html?v=' + currentdate,
                params: {
                    donorInfo: null,
                    donorid: null,
                    motcode: null,
                    lang: null,
                    children: null,
                    offset: null,
                    country: null,
                    gender: null,
                    month: null,
                    day: null,
                    ageMin: null,
                    ageMax: null,
                    ignoreMotivation: null,
                    donationPledges: null,
                    payment: null,
                    pledgetype: null,
                    descode: null
                },
                controllerAs: 'vm'
            })
            .state('sponsor', {
                url: "/sponsor/:donorid/:motcode/:lang/:pledgetype/:descode",
                controller: 'sponsorshipController',
                templateUrl: 'App/Dashboard/SponsorshipTemplate.html?v=' + currentdate,
                params: {
                    donorid: null,
                    motcode: null,
                    lang: null,
                    pledgetype: null,
                    descode: null
                },
                controllerAs: 'vm'
            })
            .state('sponsorblank', {
                url: "/sponsor",
                controller: 'sponsorshipController',
                templateUrl: 'App/Dashboard/SponsorshipTemplate.html?v=' + currentdate,
                params: {
                    donorid: null,
                    motcode: null,
                    lang: null, 
                    pledgetype: null,
                    descode: null
                },
                controllerAs: 'vm'
            })
            .state('about', {
                url: "/about",
                controller: "aboutController",
                templateUrl: 'App/Dashboard/About.html',
                controllerAs: 'va'
            });

    }

    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http', '$state', '$stateParams', 'gettextCatalog', '$window', 'editableOptions'];
    function run($rootScope, $location, $cookieStore, $http, $state, $stateParams, gettextCatalog, $window, editableOptions) {

        editableOptions.theme = 'bs3'; // bs3 - bootstrap3 theme. Can be also 'bs2', 'default'
        //gettextCatalog.setCurrentLanguage("en");
        gettextCatalog.debug = true;

        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in and trying to access a restricted page
            //debugger;
            //var restrictedPage = jQuery.inArray($location.path(), ['/login', '/sponsor']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            ////if (restrictedPage && !loggedIn) {
            if (!loggedIn) {
                var index = xIndexOf("/", $location.path(), 2);
                if (index > 0) {
                    $location.path('/login' + $location.path().substr(index, $location.path().length - index + 1));
                }
                else {
                    $location.path('/login');
                }
            }
        });

        $window.addEventListener("message", respMsg, false);

        function respMsg(e) {
            //console.log("i got an answer out! ", e);
            //console.log("i got an answer out data! ", e.data);
            var respData = eval("(" + e.data + ")");
            //console.log("i respData! ", respData);
            $rootScope.$broadcast('monerisReply', respData);
        }
    }

    function xIndexOf(Val, Str, x) {
        if (x <= (Str.split(Val).length - 1)) {
            Ot = Str.indexOf(Val);
            if (x > 1) { for (var i = 1; i < x; i++) { var Ot = Str.indexOf(Val, Ot + 1) } }
            return Ot;
        } else { return 0 }
    }

    angular.module('telemarketingApp').factory('helperService', helperService);

    helperService.$inject = ['$q', '$http'];
    function helperService($q, $http) {

        return {
            httpGetJson: httpGetJson,
            httpPostJson: httpPostJson,
            httpPutJson: httpPutJson,
            httpDeleteJson: httpDeleteJson,
            parseErrors: parseErrors,
            getServiceErrors: getServiceErrors
        };

        function httpGetJson(url) {
            var deffered = $q.defer();
            var config = {
                headers: { 'Accept': 'application/json' }
            };

            $http.get(url, config)
                .success(deffered.resolve)
                .error(deffered.reject);

            return deffered.promise;
        }

        function httpPostJson(url, body) {
            var deffered = $q.defer();
            var config = {
                headers: { 'Accept': 'application/json' }
            };

            $http.post(url, body, config)
                .success(deffered.resolve)
                .error(function (data, status) {
                    deffered.reject(data, status);
                });
            return deffered.promise;
        }

        function httpPutJson(url, body) {
            var deffered = $q.defer();
            var config = {
                headers: { 'Accept': 'application/json' }
            };

            $http.put(url, body, config)
                .success(deffered.resolve)
                .error(function (data, status) {
                    deffered.reject(data, status);
                });
            return deffered.promise;
        }

        function httpDeleteJson(url, body) {
            var deffered = $q.defer();
            var config = {
                headers: {
                    'Accept': 'application/json'
                }
            };

            $http.delete(url, body, config)
                .success(deffered.resolve)
                .error(function (data, status) {
                    deffered.reject(data, status);
                });
            return deffered.promise;
        }

        function parseErrors(errorResponse) {
            var errors = [];
            if (typeof errorResponse.message !== "undefined") {
                errors.push(errorResponse.message);
            }

            for (var key in errorResponse.modelState) {
                for (var i = 0; i < errorResponse.modelState[key].length; i++) {
                    errors.push(errorResponse.modelState[key][i]);
                }
            }
            return errors;
        }

        function getServiceErrors(errors) {
            return errors;
        }
    }
})();


// Base64 encoding service used by AuthenticationService
var Base64 = {

    keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

    encode: function (input) {
        var output = "";
        var chr1, chr2, chr3 = "";
        var enc1, enc2, enc3, enc4 = "";
        var i = 0;

        do {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
            this.keyStr.charAt(enc1) +
            this.keyStr.charAt(enc2) +
            this.keyStr.charAt(enc3) +
            this.keyStr.charAt(enc4);
            chr1 = chr2 = chr3 = "";
            enc1 = enc2 = enc3 = enc4 = "";
        } while (i < input.length);

        return output;
    },

    decode: function (input) {
        var output = "";
        var chr1, chr2, chr3 = "";
        var enc1, enc2, enc3, enc4 = "";
        var i = 0;

        // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
        var base64test = /[^A-Za-z0-9\+\/\=]/g;
        if (base64test.exec(input)) {
            window.alert("There were invalid base64 characters in the input text.\n" +
            "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
            "Expect errors in decoding.");
        }
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        do {
            enc1 = this.keyStr.indexOf(input.charAt(i++));
            enc2 = this.keyStr.indexOf(input.charAt(i++));
            enc3 = this.keyStr.indexOf(input.charAt(i++));
            enc4 = this.keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

            chr1 = chr2 = chr3 = "";
            enc1 = enc2 = enc3 = enc4 = "";

        } while (i < input.length);

        return output;
    }
};
